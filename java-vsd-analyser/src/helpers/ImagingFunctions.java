package helpers;

import java.awt.Point;
import java.awt.Polygon;
import java.awt.Shape;
import java.awt.geom.AffineTransform;
import java.awt.geom.FlatteningPathIterator;
import java.awt.geom.PathIterator;
import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;


/**
 * contains all imaging functions used in the software
 * 
 * @author Andy
 * 
 */
public class ImagingFunctions {

	/**
	 * given an array of pixels, this function returns the average value
	 * 
	 * @param pixels
	 * @return
	 */
	public static short averagePixelValue(short[] pixels) {
		if (pixels.length == 0)
			return -1;
		double avg = 0;
		for (short s : pixels)
			avg += s;

		// average of a sequence of shorts will be a short
		return (short)(avg / pixels.length);
	}

	/**
	 * calculates the average pixel value for each set of pixels
	 * 
	 * i.e. given a selection over many frames, this will return the average
	 * value of the region for every frame
	 * 
	 * @param pixelsFromEachFrame
	 * @return an array of the average pixel for each selection
	 */
	public static int[] averagePixelValues(short[][] pixelsFromEachFrame) {
		// average of selection in each frame
		int[] avg = new int[pixelsFromEachFrame.length];
		for (int i = 0; i < pixelsFromEachFrame.length; i++)
			avg[i] = averagePixelValue(pixelsFromEachFrame[i]);
		return avg;
	}

	/**
	 * used to enlarge an image by the specified scale.
	 * 
	 * repeats each pixel horizontally and vertically according to the scale
	 * 
	 * @param pixels
	 * @param width
	 * @param height
	 * @param scale
	 * @return the scaled image in pixels as short array
	 */
	public static short[] enlargeShortImage(short[] pixels, int width, int height, int scale) {
		if (pixels.length != width * height)throw new IllegalArgumentException("width and height did not match the pixel array");
		if (scale < 1) throw new IllegalArgumentException("scale must be greater than 0");
		
		int newWidth = scale * width;
		ArrayList<Short> newPixels = new ArrayList<Short>(width * scale * height * scale);
		
		for (int i = 0; i < pixels.length; i++) {
			for (int j = i; j < i + scale; j++) {
				newPixels.add(pixels[i]);
			}
			if ((i + 1) % width == 0)
			{
				for (int k = 0; k < scale - 1; k++) {
					newPixels.addAll(newPixels.subList(newPixels.size() - newWidth, newPixels.size()));
				}
			}
		}
		return convertShorts(newPixels);
	}

    /**
     * used to enlarge an image by the specified scale.
     *
     * repeats each pixel horizontally and vertically according to the scale
     *
     * @param pixels
     * @param width
     * @param height
     * @param scale
     * @return the scaled image in pixels as short array
     */
	public static int[] enlargeIntImage(int[] pixels, int width, int height, int scale)
	{
		if (pixels.length != width * height)throw new IllegalArgumentException("width and height did not match the pixel array");
		if (scale < 1) throw new IllegalArgumentException("scale must be greater than 0");
		
		int newWidth = scale * width;
		ArrayList<Integer> newPixels = new ArrayList<Integer>(width * scale * height * scale);
		
		for (int i = 0; i < pixels.length; i++) {
			for (int j = i; j < i + scale; j++) {
				newPixels.add(pixels[i]);
			}
			if ((i + 1) % width == 0)
			{
				for (int k = 0; k < scale - 1; k++) {
					newPixels.addAll(newPixels.subList(newPixels.size() - newWidth, newPixels.size()));
				}
			}
		}
		return convertInts(newPixels);
	}

    /**
     * converts an arraylist of shorts to an array
     * @param shorts
     * @return
     */
	private static short[] convertShorts(List<Short> shorts)
	{
	    short[] ret = new short[shorts.size()];
	    Iterator<Short> iterator = shorts.iterator();
	    for (int i = 0; i < ret.length; i++)
	    {
	        ret[i] = iterator.next().shortValue();
	    }
	    return ret;
	}

    /**
     * converts arraylist of ints to array
     * @param ints
     * @return
     */
	private static int[] convertInts(List<Integer> ints)
	{
	    int[] ret = new int[ints.size()];
	    Iterator<Integer> iterator = ints.iterator();
	    for (int i = 0; i < ret.length; i++)
	    {
	        ret[i] = iterator.next().intValue();
	    }
	    return ret;
	}
	
	/**
	 * scales a point from one resolution to another, maintaining the relative position
	 *  
	 * 
	 * newX = (oldX / oldLength) * newLength
	 * newY = (oldY / oldHeight) * newHeight
	 */
	public static Point scalePoint(Point2D point, int oldLength, int oldHeight, int newLength, int newHeight)
	{
		int newX = scaleVal((int)point.getX(), oldLength, newLength);
		int newY = scaleVal((int)point.getY(), oldHeight, newHeight);
		return new Point(newX, newY);
	}
	
	/**
	 * scales a value to the new Size
	 * 
	 * newVal = oldVal / oldSize * newSize
	 * 
	 * useful when mapping an X or Y value from one resolution image to a different one to
	 * maintain the relative position
	 * @return the scaled 
	 */
	public static int scaleVal(int val, int oldSize, int newSize)
	{
		return (int)((double)(val) / oldSize * newSize);
	}
	
	/**
	 * returns an array of pixels held within the polygon's coordinates.
	 * 
	 * runs an O(pixelCount) algorithm - not recommended for large images
	 * 
	 * @param region the polygon to select pixels from 
	 * @param image the image which contains the polygon
	 * @return
	 */
	public static short[] polygonPixels(Shape region, short[] image, int width)
	{
		if (!polygonInsideRectangle(region, image, width)) throw new IllegalArgumentException("region must be inside the image");
		List<Short> pixels = new ArrayList<>();

		for (int i = 0; i < image.length; i++) {
			Point point = getPointFromIntPos(i, width);
			if (region.contains(point))
				pixels.add(image[i]);
		}
		return convertShorts(pixels);
	}

    /**
     * determines if a given shape is inside some rectangle
     * @return
     */
	public static boolean polygonInsideRectangle(Shape shape, short[] image, int width)
	{
		List<Point> points = getPointsFromShape(shape);
		for (Point point : points) {
			int index = getIntPosFromPoint(point, width);
			if (index >= image.length || index < 0)
			{
				return false;
			}
		}
		return true;
	}

    /**
     * returns an arrayList of points making up the perimeter of a polygon
     * @param shape
     * @return
     */
	public static List<Point> getPointsFromShape(Shape shape)
	{
		FlatteningPathIterator iter=new FlatteningPathIterator(shape.getPathIterator(new AffineTransform()), 1);		
		List<Point> points = new ArrayList<Point>();
        float[] coords=new float[6];
		while (!iter.isDone())
		{
			iter.currentSegment(coords);
			int x = (int)coords[0];
			int y = (int)coords[1];
			points.add(new Point(x, y));
			iter.next();
		}
		return points;
	}

    /**
     * returns the central point of a shape
     * @param shape
     * @return
     */
	public static Point getMiddleOfShape(Shape shape)
	{
		List<Point> points = ImagingFunctions.getPointsFromShape(shape);
		int middleX = 0;
		int middleY = 0;
		for (Point point : points) {
			middleX+= point.getX();
			middleY+= point.getY();
		}
		middleX/= points.size();
		middleY/= points.size();
		// return the middle value so the label can be placed there
		return new Point(middleX, middleY);
		
	}
	
	/**
	 * converts a position of a 1D array two a Point containing an X and Y position
	 * @param pos the index in the 1D array
	 * @param width the width of the image
	 * @return the 
	 */
	public static Point getPointFromIntPos(int pos, int width)
	{
		if (width < 1) throw new IllegalArgumentException("width cannot be < 1");
		if (pos < 0) throw new IllegalArgumentException("index cannot be < 1");
		int x = pos % width;
		int y = (pos - x) / width;
		return new Point(x,y);
	}
	
	/**
	 * 
	 * given 2D point of a pixel, the function returns the int position used
	 * for a 1D array of pixels.
	 * 
	 * @param point the Point of the pixel to translate e.g. (4,1)
	 * @param width the width of the image
	 * @return the integer position of the point
	 */
	public static int getIntPosFromPoint(Point point, int width)
	{
		int x = point.x;
		int y = point.y;
		return y * width + x;
	}

    /**
     * returns a zeroed array of length size
     */
	public static short[] initialiseShortArray(int size)
	{
		short[] data = new short[size];
		for (int i = 0; i < size; i++) {
			data[i] = 0; 
		}
		return data;
	}
	
	/**
	 * adds the specified padding to the data
	 * the padding will be added to boths ends of the data e.g. oldWidth = 4, padding = 2, newWidth = 8
	 * @param data the data to be padded
	 * @param width the old Width
	 * @param height the old height
	 * @param widthPadding the padding to add to the left and right side of the image
	 * @param heightPadding the padding to add to the top and bottom of the image
	 * @return the padded image
	 */
	public static short[] addPaddingToImage(short[] data, int width, int height, int widthPadding, int heightPadding)
	{
		if (width * height != data.length) throw new IllegalArgumentException("Width and height did not match the pixel array");
		if (widthPadding < 0 || heightPadding < 0) throw new IllegalArgumentException("Invalid padding values: must be > 0");
		int newWidth = width + widthPadding * 2;
		int newHeight = height + heightPadding * 2;
		// padding goes on each side, so we're multiplying by 2
		short[] newData = ImagingFunctions.initialiseShortArray(newWidth * newHeight);

		// where the data should be input
		int pos = newWidth * heightPadding + widthPadding;
		for (int i = 0; i < width * height; i++) {
			newData[pos] = data[i];
			if ((i + 1) % width == 0)
			{
				pos+= widthPadding * 2 + 1;
			}
			else {
				pos++;
			}
		}
		return newData;
	}
}