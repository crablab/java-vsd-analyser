package helpers;

import org.apache.commons.math3.stat.regression.SimpleRegression;
import org.jfree.data.xy.XYDataItem;
import org.jfree.data.xy.XYSeries;

import vsdController.VSDController;

/**
 * Extended by Benjamin Kidd from 09/07/2014
 * <p/>
 * Created by Andy on 27/03/2014.
 * <p/>
 * Adapted from Jannetta Steyn's SeriesFilter.java @ Newcastle University
 * <p/>
 * Class containing several filtering functions for use with the XYSeries from the JFreeChart library
 */
public class Filters {


	
	
    /**
     * performs a high pass filter on the XYSeries
     *
     * @param x     the series to filter
     * @param alpha strength of the filter
     * @return filtered series
     */
    public static XYSeries highPass(XYSeries x, double alpha) {

        int n = x.getItemCount();
        XYSeries y = new XYSeries(x.getKey() + " HP");
        double a = alpha;
        y.add(x.getDataItem(0));
        for (int i = 1; i < n; i++) {
            // y[i] := a * y[i-1] + a * (x[i] - x[i-1])
            y.add(x.getDataItem(i).getXValue(), (a
                    * x.getDataItem(i - 1).getYValue()
                    + a
                    * (x.getDataItem(i).getYValue() - x.getDataItem(i - 1)
                    .getYValue())));
        }
        return y;
    }

    /**
     * performs a band-pass filter on the XYSeries
     *
     * @param x     series to filter
     * @param lpVal strength of low-pass filter
     * @param alpha strength of high-pass filter
     * @return filtered series
     */
    public static XYSeries bandPass(XYSeries x, int lpVal, double alpha) {
        String key = x.getKey() + " BP";
        XYSeries series = lowPass(highPass(x, alpha), lpVal);
        series.setKey(key);
        return series;
    }

    /**
     * performs a low-pass filter on the series
     *
     * @param x     the XYSeries to filter
     * @param frame the frame length of the filter (i.e. the strength)
     * @return the filtered series
     */
    public static XYSeries lowPass(XYSeries x, int frame) {
        if (frame < 1) return null;
        frame *= 2;
        XYSeries y = new XYSeries(x.getKey().toString() + "  LP");
        int N = x.getItemCount();
        for (int i = 0; i < N; i++) {
            int start = i - (frame / 2);
            int stop = i + (frame / 2);
            double avg = 0;
            if (start < 0) start = 0;
            if (stop > N) stop = N;
            for (int k = start; k < stop; k++) {
                avg += x.getDataItem(k).getYValue();
            }
            int stopSubStart = (stop - start) <= 0 ? 1 : stop - start;
            y.add(x.getDataItem(i).getX(), avg / stopSubStart);
        }
        return y;
    }

    /**
     * de-trends the XYSeries
     * <p/>
     * calculates the line of best fit and removes the value of the intersection on the Y axis from each point.
     * Also calculates the slope of the data and removes it from each point.
     * <p/>
     * Result is data varying round the 0 value on the Y axis, and the line of best fit being horizontally flat
     *
     * @param inSeries XYSeries to de-trend
     * @param j 
     * @return de-trended series
     */
    //TODO ask jannetta what she wants to alter manually
        public static XYSeries detrend(XYSeries inSeries, int j) {
        SimpleRegression regression = new SimpleRegression();
        XYDataItem dataItem;
        for (int i = 0; i < inSeries.getItemCount(); i++) {
            dataItem = inSeries.getDataItem(i);
            regression.addData(dataItem.getXValue(), dataItem.getYValue());
        }
        double slope = regression.getSlope();
        double intercept = regression.getIntercept();
        XYSeries newSeries = new XYSeries(inSeries.getKey());
        for (int i = 0; i < inSeries.getItemCount(); i++) {
            dataItem = inSeries.getDataItem(i);
            newSeries.add(dataItem.getXValue(), dataItem.getYValue() - intercept - (dataItem.getXValue() * slope));
        }
        return newSeries;
    }
    /**
     * 	Normalises an XYSeries
     * 
     * @param data_set		XYSeries to normalise
     * @param normal_min	Normalisation min value
     * @param normal_max	Normalisation max value
     * @return normalized series
     */
    public static XYSeries normalize(XYSeries data_set, int normal_min, int normal_max){
    	double A = data_set.getMinY();
    	double B = data_set.getMaxY();
    	int a = normal_min;
    	int b = normal_max;
    	XYDataItem dataItem;
    	XYSeries newData = new XYSeries(data_set.getKey() + "NRM");
    	//TODO this seems to inverse the data. fix it
    	for(int i=0; i<data_set.getItemCount();i++){
    		dataItem = data_set.getDataItem(i);
    		newData.add(dataItem.getXValue(), ((a+(((dataItem.getYValue()-A)*(b-a))/(B-A))))*(-1));
    	}
    	return newData;
    	
    }
    /**
     * 	Smoothes an XYSeries
     * 
     * @param data_set		XYSeries to Smooth
     * @param window		smoothing window
     * @return smoothed series
     */
    public static XYSeries smooth(XYSeries data_set, int window){
    	int length =(data_set.getItemCount());
    	//Position
    	int s = 1;
    	int e = 1;
    	XYSeries y = new XYSeries(data_set.getKey() + "SM");
    	//loop for every window	
    	for(s=1;s<length;s+=window){
    		e+=window;
    		if(e>length)e=length;
    		
    		short c = 0;
    		for(int j =s;j<e;j++){
    			c+=data_set.getDataItem(j).getYValue();
    		}
    		c/=window;
    		double sampTime = data_set.getDataItem((s+(window/2))).getXValue();
    		y.add(sampTime, c);
    	}
    		

     return y;
    }

}
