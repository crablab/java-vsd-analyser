package helpers;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

/**
 * Contains methods which may be useful when extracting data from binary files
 */
public class ByteConversions {

	/**
	 * converts two bytes into their signed short value
	 * 
	 */
	public static short getShortFromBytes(byte byte1, byte byte2)
	{
		ByteBuffer bb = ByteBuffer.allocate(2);
		bb.order(ByteOrder.LITTLE_ENDIAN);
		bb.put(byte1);
		bb.put(byte2);

		return bb.getShort(0);
	}

	
	/**
	 * converts 4 bytes into their respective float value
	 * 
	 */
	public static float getFloatFromBytes(byte byte1, byte byte2, byte byte3, byte byte4)
	{
		ByteBuffer bb = ByteBuffer.allocate(4);
		bb.order(ByteOrder.LITTLE_ENDIAN);
		bb.put(byte1);
		bb.put(byte2);
		bb.put(byte3);
		bb.put(byte4);

		return bb.getFloat(0);

	}
	
	/**
	 * converts a given number of bytes into their char equivalent
	 * @param byteArray
	 * @return
	 */
	public static char[] getCharArrayFromByteArray(byte[] byteArray)
	{
		return new String(byteArray).toCharArray();
	}

}
