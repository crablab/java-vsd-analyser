package app;

import vsdController.VSDController;
import vsdView.MainWindow;

import javax.swing.*;

public class Driver {

	public static void main(String[] args)
	{
        try {
            SwingUtilities.invokeLater(new Runnable() {

                @Override
                public void run() {
                    runApp();
                }
            });
        }
        catch (Exception e)
        {
            JOptionPane.showMessageDialog(null, "Unexpected error occurred. Closing down.");
        }
	}
	
	public static void runApp()
	{
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (Exception e){
			e.printStackTrace();
		}
		MainWindow vieWindow = new MainWindow();
		VSDController controller = new VSDController(vieWindow);
		vieWindow.setController(controller);
	}

}