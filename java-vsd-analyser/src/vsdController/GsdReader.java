package vsdController;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.Arrays;

import javax.swing.JOptionPane;

import vsdModel.AnalogInputSignal;
import vsdModel.ControlInfo;
import vsdModel.DataFormatInfo;
import vsdModel.DifferentialData;
import vsdModel.GsdData;
import vsdModel.InputSignalInfo;
import vsdModel.VSDFrame;
import vsdView.MainWindow;

/**
 * 
 * @author Andy Stabler
 * 
 * Class to read all data from the .gsd files
 *
 */
public class GsdReader {
	
	public static final int HEADER_START = 0;
	public static final int HEADER_LENGTH = 256;
	public static final int FORMAT_START = 256;
	public static final int FORMAT_LENGTH = 72;
	public static final int INPUT_SIGNAL_START = 328;
	public static final int INPUT_SIGNAL_LENGTH = 20;
	public static final int CONTROL_START = 348;
	public static final int CONTROL_LENGTH = 624;
	public static final int BACKGROUND_START = 972;
	
	private GsdData gsdData;

	/**
	 * @return the gsdData
	 */
	public GsdData getGsdData() {
		return gsdData;
	}

	public GsdReader(String filePath) throws FileNotFoundException {
		gsdData = readFile(filePath);
	}
	
	public GsdData readFile(String filePath) throws FileNotFoundException {
		if (filePath == null || filePath.equals(""))
			throw new IllegalArgumentException("ERROR: GSD file path was invalid");
		if (!new File(filePath).exists()) throw new FileNotFoundException("File " + filePath + " was not found");
		GsdData newGsdData = new GsdData();

		RandomAccessFile rGsdFile = null;
		try {

			File binaryGsdFile = new File(filePath);
			rGsdFile = new RandomAccessFile(binaryGsdFile, "r");

			byte[] header = getBytesFromFile(HEADER_START, HEADER_LENGTH, rGsdFile);

			byte[] dataFormatInfoBytes = getBytesFromFile(FORMAT_START, FORMAT_LENGTH, rGsdFile);
			DataFormatInfo dFormatInfo = DataFormatInfo.decodeFormatInfo(dataFormatInfoBytes);
			
			
			byte[] inputSignal = getBytesFromFile(INPUT_SIGNAL_START, INPUT_SIGNAL_LENGTH, rGsdFile);
			InputSignalInfo inputSignalInfo = InputSignalInfo.decodeInputSignalInfo(inputSignal);
			byte[] control = getBytesFromFile(CONTROL_START, CONTROL_LENGTH, rGsdFile);
			ControlInfo controlInfo = ControlInfo.decodeControlInfo(control);
			// background image data is Short (16bit signed binary), hence the *2
			int backgroundSize = dFormatInfo.getnDataXSize() * dFormatInfo.getnDataYSize() * 2;
			byte[] currentFrameBytes = getBytesFromFile(BACKGROUND_START, backgroundSize, rGsdFile);
			VSDFrame backgroundImage = VSDFrame.extractDifferenceFrame(currentFrameBytes, dFormatInfo.getnDataXSize(), dFormatInfo.getnDataYSize());

			int xSize = backgroundImage.getxSize();
			int ySize = backgroundImage.getySize();

			
			// remember the *2 is because the data is 16 bit signed short 
			// looking after the background image
			int currentFrameStartPos = BACKGROUND_START + backgroundSize * 2;
			
			/* 
			 * start looking at the rest of the frames
			 */
			
			VSDFrame[] frames = getDifferentialDataFromBytes(backgroundImage, dFormatInfo, rGsdFile, currentFrameStartPos);
			//Analog input signal data
			//972+X*Y*2*( n+1)
			int analogStartPos = BACKGROUND_START + xSize * ySize * 2 * dFormatInfo.getnFrameSize();
			int analogSize = inputSignalInfo.getnChanum() * inputSignalInfo.getnFrameSize() * inputSignalInfo.getnRate() * 2;
			
			System.out.println("ana size:"+analogSize);
			byte[] anologSignal = getBytesFromFile(analogStartPos, analogSize, rGsdFile);
			AnalogInputSignal analogInputSignal = AnalogInputSignal.decode(anologSignal, analogSize);
			
			newGsdData.setFileName(filePath);
			newGsdData.setHeaderInformation(new String(header));
			newGsdData.setFormInfo(dFormatInfo);
			newGsdData.setInputSignalInfo(inputSignalInfo);
			newGsdData.setControlInfo(controlInfo);
			newGsdData.setBackgroundImage(backgroundImage);
			newGsdData.setFrames(frames);
			newGsdData.setAnalogInputSignal(analogInputSignal);
		} catch (IOException e) {
			System.out.println("ERROR reading gsd file . . . ");
			e.printStackTrace();
		} finally {
			if (rGsdFile != null) {
				try {
					rGsdFile.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return newGsdData;
	}

    /**
     * returns each frame of the video.
     * <p>calculates each frame using the differential value (adds each difference val to the respective pixel in the background image to get new pixel value</p>
     * @param backgroundImage
     * @param dFormatInfo
     * @param rGsdFile
     * @param startPos
     * @return array of frames in the video
     */
	private VSDFrame[] getDifferentialDataFromBytes(VSDFrame backgroundImage, DataFormatInfo dFormatInfo, RandomAccessFile rGsdFile, int startPos)
	{
		byte[] currentFrameBytes = null;
		VSDFrame[] frames = new VSDFrame[dFormatInfo.getnFrameSize()];
		int currentFrameStartPos = startPos;
		int backgroundSize = backgroundImage.getSize() * 2; // short
		for (int i = 0; i < dFormatInfo.getnFrameSize(); i++)
		{
			try{
				// looking at the frame AFTER the background image
				currentFrameBytes = getBytesFromFile(currentFrameStartPos, backgroundSize, rGsdFile);
				
				
				frames[i] = DifferentialData.adjustImage(backgroundImage, VSDFrame.extractDifferenceFrame(currentFrameBytes, dFormatInfo.getnDataXSize(), dFormatInfo.getnDataYSize()).getData());
				currentFrameStartPos+= backgroundSize;
			}catch(Exception e){
				System.out.println("FRAME COUNT ON ERROR: " + i);
				e.printStackTrace();
				System.exit(-1);
			}
		}
		return frames;
	}

	/**
	 * 
	 * @param startPos
	 *            the position of the first byte to look at
	 * @param byteCount
	 *            the number of bytes to take
	 * @param file
	 *            the file you'd like to read
	 * @return the byte array of data
	 */
	private static byte[] getBytesFromFile(int startPos, int byteCount,
			RandomAccessFile file) {
		
		byte[] data = null;
		try {
			file.seek(startPos);
			data = new byte[byteCount];
			file.read(data);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return data;
	}
}
