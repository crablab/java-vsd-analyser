package vsdController;

import vsdModel.GshData;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Used to read the data held in a GSH file and store the information in a format we can access and alter.
 * @author Andy Stabler
 *
 */
public class GshReader implements Reader {
	
	public int readErrors = 0;

	public static final String DATA_NAME_STRING = "DataName";
	public static final String ACQUISITION_DATE_STRING = "AcquisitionDate";
	public static final String DATA_SIZE_STRING = "Data Size";
	public static final String FRAME_SIZE_STRING = "Frame Size";
	public static final String SAMPLING_TIME_STRING = "Sampling Time";
	public static final String AVERAGING_STRING = "Averaging";
	public static final String CMT_STRING_STRING = "Cmt";
	public static final String CAMERA_STRING = "Camera";
	public static final String XPX_STRING = "xpx";
	public static final String YPX_STRING = "ypx";
	public static final String CHANNEL_STRING = "Chanel";
	public static final String MIRROR_STRING = "Mirror";
	public static final String FRAMES_STRING = "frames";
	public static final String SAMPLING_STRING = "Sampling";
	public static final String TRIGGER_STRING = "Trigger";
	public static final String PAGE_MODE_STRING = "PageMode";
	public static final String AVERAGE_STRING = "Average";
	public static final String CHANNEL_1_STRING = "CH1";
	public static final String CHANNEL_2_STRING = "CH2";
	public static final String DELAYED_STIMULATION_STRING = "Delayed stimulation";
	public static final String SHUTDLY_STRING = "Shutdly";
	public static final String REFERENCE_AVERAGE_STRING = "Refrence avergae";
	
	private Map<String, String> fields;
	
	/**
	 * @return the fields
	 */
	public Map<String, String> getFields() {
		return fields;
	}

	private GshData gshData;
	
	/**
	 * @return the gshData
	 */
	public GshData getGshData() {
		return gshData;
	}

	public GshReader(String filePath)
	{
		readFile(filePath);
	}
	
	public GshReader()
	{
		initialiseFields();
	}
	
	private void initialiseFields()
	{
		fields = new HashMap<String, String>(){
			{
				put(DATA_NAME_STRING, "");
				put(ACQUISITION_DATE_STRING, "");
				put(DATA_SIZE_STRING, "");
				put(FRAME_SIZE_STRING, "");
				put(SAMPLING_TIME_STRING, "");
				put(AVERAGING_STRING, "");
				put(CMT_STRING_STRING, "");
				put(CAMERA_STRING, "");
				put(XPX_STRING, "");
				put(YPX_STRING, "");
				put(CHANNEL_STRING, "");
				put(MIRROR_STRING, "");
				put(FRAMES_STRING, "");
				put(SAMPLING_STRING, "");
				put(TRIGGER_STRING, "");
				put(PAGE_MODE_STRING, "");
				put(AVERAGE_STRING, "");
				put(CHANNEL_1_STRING, "");
				put(CHANNEL_2_STRING, "");
				put(DELAYED_STIMULATION_STRING, "");
				put(SHUTDLY_STRING, "");
				put(REFERENCE_AVERAGE_STRING, "");
			}
		};
	}

	/* (non-Javadoc)
	 * @see logic.Reader#readGSHFile(java.lang.String)
	 */
	@Override
	public void readFile(String path) {
		initialiseFields();
		if (path == null || path.equals(""))
			throw new IllegalArgumentException("ERROR: GSH file path was invalid");
		readErrors = 0;
		
		try (BufferedReader bReader = new BufferedReader(new FileReader(path))) {
			String currentLine;

			while ((currentLine = bReader.readLine()) != null) {
				decodeAndAssign(currentLine);
			}
		} catch (IOException e) {
			System.out.println("ERROR reading gsh file . . . ");
			e.printStackTrace();
		}
		gshData = generateGshData();
	}

	/**
	 * GSH files lack consistency and they contain spelling mistakes, for this reason decoding is 
	 * slightly more involved than it should be.
	 * 
	 * @param stringToDecode the string that contains a field and value, e.g. "field = value"
	 */
	protected void decodeAndAssign(String stringToDecode) {
		
		SeparatorEnum separatorEnum =  getSeperator(stringToDecode);
		if (separatorEnum == SeparatorEnum.NA)
		{
			// unusual case where there is no separator. 
			if (stringToDecode.toLowerCase().contains(REFERENCE_AVERAGE_STRING.toLowerCase()))
			{
				assignValue(REFERENCE_AVERAGE_STRING,
						stringToDecode.substring(
								stringToDecode.toLowerCase()
								.indexOf(REFERENCE_AVERAGE_STRING.toLowerCase())
								+ REFERENCE_AVERAGE_STRING.length()
								, stringToDecode.length()
								).trim()
							);
			}
			return;
		}
		
		boolean splitByEquals = separatorEnum == SeparatorEnum.EQUALS_SEP;
		
		String[] splitDataStrings = stringToDecode.split(splitByEquals ? "=" : ":");
		
		// we need at least two strings; name and value
		if (splitDataStrings.length < 2)
			return;
		
		String fieldName = splitDataStrings[0].trim();
		String fieldValue = "";
		
		// for some reason xpx and ypx are on the same line, this is a special case
		if (fieldName.toLowerCase().equals(XPX_STRING.toLowerCase()))
		{
			String xpxString = splitDataStrings.length > 1 ? splitDataStrings[1].trim().split(" ")[0] : "-1";
			assignValue(XPX_STRING, xpxString);
			String ypxString = splitDataStrings.length > 2 ? splitDataStrings[2].trim() : "-1";
			assignValue(YPX_STRING, ypxString);
			return;
		}
		
		// maybe use string builder here: not sure how long the value could be
		for (int i = 1; i < splitDataStrings.length; i++) {
			fieldValue = fieldValue.concat(splitDataStrings[i]);
		}
		
		
		if (!assignValue(fieldName, fieldValue.trim()))
		{
			System.out.println("ERROR reading line. " + stringToDecode);
		}
	}

	/**
	 * determines the separator used to split the field from the value, e.g. 'field = value' the seperator is EQUALS_SEP
	 * @param inStr
	 * @return
	 */
	public static SeparatorEnum getSeperator(String inStr) {
		int equalsIndex = inStr.indexOf("=");
		int colonIndex = inStr.indexOf(":");

		// neither separators were located in the string
		if (equalsIndex == -1 && colonIndex == -1)
			return SeparatorEnum.NA;
		if (equalsIndex != -1) {
			return SeparatorEnum.EQUALS_SEP;
		} else if (colonIndex != -1) {
			return SeparatorEnum.COLON_SEP;
		} else {
			return equalsIndex < colonIndex ? SeparatorEnum.EQUALS_SEP
					: SeparatorEnum.COLON_SEP;
		}
	}
	
	private boolean assignValue(String name, String value)
	{
		if (fields == null)
			throw new IllegalStateException("fields must be initialised before calling this method");
		for (String keyName : fields.keySet()) {
			if (name.equalsIgnoreCase(keyName))
			{
				fields.put(name, value);
				return true;
			}
		}
		return false;
	}
	
	/**
	 * sets the gshData's values based on the values read from the hashmaps
	 */
	public GshData generateGshData()
	{
		GshData gshData = new GshData();
		if (fields == null)
			throw new IllegalStateException("fields must be initialised before calling this method");
		gshData.setDataName(fields.get(DATA_NAME_STRING));
		try {
			gshData.setAquisitionDate(new SimpleDateFormat("yyyy/MM/dd HH:mm:ss").parse(fields.get(ACQUISITION_DATE_STRING)));
		} catch (ParseException e) {
			gshData.setAquisitionDate(new Date(Long.MIN_VALUE));
			readErrors++;
		}
		gshData.setDataSize(fields.get(DATA_SIZE_STRING));
		gshData.setFrameSizeString(fields.get(FRAME_SIZE_STRING));
		gshData.setSamplingTime(fields.get(SAMPLING_TIME_STRING));
		try
		{
			gshData.setAveraging(Double.parseDouble(fields.get(AVERAGING_STRING)));
		} catch(Exception e)
		{
			gshData.setAveraging(0.0);
			readErrors++;
		}
		gshData.setCmt(fields.get(CMT_STRING_STRING));
		gshData.setCamera(fields.get(CAMERA_STRING));
		try
		{
			gshData.setXpx(Integer.parseInt(fields.get(XPX_STRING)));			
		} catch(NumberFormatException e)
		{
			gshData.setXpx(-1);
			readErrors++;
		}
		try{
			gshData.setYpx(Integer.parseInt(fields.get(YPX_STRING)));
		} catch(NumberFormatException e)
		{
			gshData.setYpx(-1);
			readErrors++;
		}
		gshData.setChannel(fields.get(CHANNEL_STRING));
		gshData.setMirror(fields.get(MIRROR_STRING));
		gshData.setFrames(fields.get(FRAMES_STRING));
		gshData.setSampling(fields.get(SAMPLING_STRING));
		gshData.setTrigger(fields.get(TRIGGER_STRING));
		gshData.setPageMode(fields.get(PAGE_MODE_STRING));
		gshData.setAverage(fields.get(AVERAGE_STRING));
		gshData.setCh1(fields.get(CHANNEL_1_STRING));
		gshData.setCh2(fields.get(CHANNEL_2_STRING));
		gshData.setDelayedStimulation(fields.get(DELAYED_STIMULATION_STRING));
		gshData.setShutdly(fields.get(SHUTDLY_STRING));
		gshData.setReferenceAverage(fields.get(REFERENCE_AVERAGE_STRING));
		initialiseFields();
		return gshData;
	}

	/**
	 * 
	 * @author Andy Stabler
	 *
	 */
	public enum SeparatorEnum {
		EQUALS_SEP, COLON_SEP, NA
	}



}
