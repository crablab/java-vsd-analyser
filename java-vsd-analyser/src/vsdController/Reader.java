package vsdController;

public interface Reader {

	/**
	 * reads the data in the file located at 'path' and creates a GSx object from its content
	 * @param path
	 */
	public abstract void readFile(String path);

}