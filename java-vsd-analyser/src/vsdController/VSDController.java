package vsdController;

import helpers.ImagingFunctions;
import vsdModel.GsdData;
import vsdModel.VSDData;
import vsdModel.VSDFrame;
import vsdView.AnalysisChart;
import vsdView.AnalysisPanel;
import vsdView.MainWindow;
import vsdView.Region;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.io.FileNotFoundException;
import java.io.IOException;

/**
 * Controller from the system, used for the communication between the view and the model
 *
 */
public class VSDController {

	private VSDData lowRes;
	private VSDData highRes;
	private MainWindow vsdView;
	
	private Timer timer;
	private int timerIterationCount;
	
	/**
	 * @return the lowRes
	 */
	public VSDData getLowRes() {
		return lowRes;
	}

	/**
	 * @param lowRes
	 *            the lowRes to set
	 */
	public void setLowRes(VSDData lowRes) {
		this.lowRes = lowRes;
		if (vsdView != null)
			vsdView.setLowResData(lowRes);
		vsdView.refreshImages();
        vsdView.imageAdjustments.setLowResData(lowRes);
	}

	/**
	 * @return the highRes
	 */
	public VSDData getHighRes() {
		return highRes;
	}

	/**
	 * @param highRes
	 *            the highRes to set
	 */
	public void setHighRes(VSDData highRes) {
		this.highRes = highRes;
		if (vsdView != null)
			vsdView.setHighResData(highRes);
		vsdView.refreshImages();
        vsdView.imageAdjustments.setHighResData(highRes);
	}

	public VSDController(MainWindow view) {
		// the user won't have loaded the data on start up, set to null
		lowRes = null;
		highRes = null;
		vsdView = view;
		timerIterationCount = 0;
		vsdView.addFireHighResListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				String path = e.getActionCommand();
				// set view's VSDdata to be the model (the read one)
				VSDData vsdData = null;
				try {
					vsdData = getVSDDataFromPath(path);
				} catch (FileNotFoundException e1) {
					e1.printStackTrace();
					JOptionPane.showMessageDialog(null, e1.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
				}
				if (vsdData != null)
				{
					int xSize = vsdData.getMainData() != null ? vsdData.getMainData().getFormInfo().getnDataXSize() : -1;
					int ySize = vsdData.getMainData() != null ? vsdData.getMainData().getFormInfo().getnDataYSize() : -1;
					if (xSize != MainWindow.ORIGINAL_HIGH_RES_IMAGE_WIDTH || ySize != MainWindow.ORIGINAL_HIGH_RES_IMAGE_HEIGHT)
					{
						vsdView.showErrorMessage("Error loading file. Are you sure you've selected the High-Res data?");
						return;
					}
					setHighRes(vsdData);
					vsdView.setHighResFileLoc(path);//highResFileName.setText(path);
					vsdView.highLowInfoPanel.addHighResData(vsdData.getMainData());
                    vsdView.imageAdjustments.setEnabled(true);
                    vsdView.imageAdjustments.updateImages();
				}
			}
		});
		vsdView.addFireLowResListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				vsdView.analysisPanel.clearCharts(true);
				String path = e.getActionCommand();
				// set view's VSDdata to be the model (the read one)
				VSDData vsdData = null;
				try {
					vsdData = getVSDDataFromPath(path);
				} catch (FileNotFoundException e1) {
				
					JOptionPane.showMessageDialog(null, e1.getMessage());
				}
				if (vsdData != null)
				{
					int xSize = vsdData.getMainData() != null ? vsdData.getMainData().getFormInfo().getnDataXSize() : -1;
					int ySize = vsdData.getMainData() != null ? vsdData.getMainData().getFormInfo().getnDataYSize() : -1;
					if (xSize != MainWindow.ORIGINAL_LOW_RES_IMAGE_WIDTH || ySize != MainWindow.ORIGINAL_LOW_RES_IMAGE_HEIGHT){
						vsdView.showErrorMessage("Error loading file. Are you sure you've selected the Low-Res data?");
						return;
					}
					
					setLowRes(vsdData);
					vsdView.setLowResFileLoc(path);//lowResFileName.setText(path);
					vsdView.highResOverlay.clearData();
					vsdView.analysisPanel.clearCharts(true);
					//NOTE
					System.out.println("VSDCON actionperf");
					vsdView.analysisPanel.makeChart(lowRes.getMainData().getAnalogInputSignal().getSignalData(), AnalysisPanel.ANALOG_NAME, vsdData.getMainData().getFormInfo().getdSampleTime());
					vsdView.highLowInfoPanel.addLowResData(vsdData.getMainData());
					vsdView.playLowResFrames.setEnabled(true);
                    vsdView.imageAdjustments.setEnabled(true);
                    vsdView.imageAdjustments.updateImages();
				}
			}
		});
		
		vsdView.selectionTools.addPolygonSelectionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				vsdView.selectionTools.selectionEnabled = !vsdView.selectionTools.selectionEnabled;
				vsdView.highResOverlay.setEnabled(vsdView.selectionTools.selectionEnabled);
				vsdView.selectionTools.shapeRemovalbtn.setEnabled(!vsdView.selectionTools.selectionEnabled);
			}
		});
		
		vsdView.selectionTools.addClearAllSelectionsListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				vsdView.highResOverlay.clearData();
				vsdView.analysisPanel.clearCharts(false);
			}
		});

		vsdView.selectionTools.addShapeRemoveListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				vsdView.selectionTools.selectionEnabled = !vsdView.selectionTools.selectionEnabled;
				vsdView.selectionTools.polygonSelectionbtn.setEnabled(!vsdView.selectionTools.selectionEnabled);
				vsdView.highResOverlay.setErasing(vsdView.selectionTools.selectionEnabled);
			}
		});

		vsdView.addRefreshListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				//updateCharts();
				refreshCharts();
			}
		});
		
		vsdView.addPlayListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				System.out.println("frame count: " + lowRes.getMainData().getFormInfo().getnFrameSize());
				vsdView.togglePlayButton();
				// new toggle value says we're playing images? then play them
				timerIterationCount = 0;
				if (vsdView.playingLowRes)
				{
					timer = new Timer((int)lowRes.getMainData().getFormInfo().getdSampleTime(), null);
					timer.addActionListener(new ActionListener() {
						
						@Override
						public void actionPerformed(ActionEvent evt) {
							System.out.println(timerIterationCount + " < " + lowRes.getMainData().getFrames().length + (timerIterationCount < lowRes.getMainData().getFrames().length));
							if (timerIterationCount < lowRes.getMainData().getFrames().length)
							{
								timerTick();
                                vsdView.analysisPanel.videoTick(timerIterationCount);
                                vsdView.imageAdjustments.updateLowRes();
							}
							else
							{
								timerIterationCount = 0;
								timerTick();
								((Timer)evt.getSource()).stop();
								timer.stop();
                                vsdView.analysisPanel.videoStop();
                                vsdView.imageAdjustments.updateLowRes();
							}
						}
					});
					timer.start();
				}
				else {
					timerIterationCount = 0;
					timerTick();
					timer.stop();
                    vsdView.analysisPanel.videoStop();
                    vsdView.imageAdjustments.updateLowRes();
				}
			}
		});

        vsdView.graphAdjustments.addAutoDetrendListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent e) {
                AnalysisChart.autoDetrend = ((JCheckBox)e.getSource()).isSelected();
            }
        });
        
        vsdView.graphAdjustments.addAutoNormalizeListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent e) {
                AnalysisChart.autoNormalize = ((JCheckBox)e.getSource()).isSelected();
            }
        });
        
        vsdView.graphAdjustments.addAutoSmoothListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent e) {
                AnalysisChart.autoSmooth = ((JCheckBox)e.getSource()).isSelected();
            }
        });
	}
	
	private void timerTick()
	{
		try{
			vsdView.lowResImagePanel.updateImageAndTime(
                    MainWindow.makeImage(
                            lowRes.getMainData().getFrames()[timerIterationCount].getData(), MainWindow.LOW_RES_IMAGE_WIDTH, MainWindow.LOW_RES_IMAGE_HEIGHT)
                    , lowRes.getMainData().getFormInfo().getdSampleTime() * timerIterationCount
            );
			System.out.println("Updating . . .");
            vsdView.analysisPanel.videoTick(timerIterationCount);
			timerIterationCount++;
		} catch (IOException e)
		{
			vsdView.showErrorMessage("error showing frame! Aborting...");
			return;
		}
		
	}

	public void refreshCharts()
	{
		System.out.println("refreshing charts");
		vsdView.analysisPanel.clearCharts(false);
		for (Region region : vsdView.lowResOverlay.getRegions()) {
			addChart(region.getShape(), AnalysisPanel.ID_PREFIX + region.getID());
		}
	}
	
	public void addChart(Shape shape, String label)
	{
		if (lowRes == null)
		{
			vsdView.showErrorMessage("There is no data perform the selection on. Try uploading the low-resolution file");
			return;
		}
		int frameCount = lowRes.getMainData().getFormInfo().getnFrameSize();
		//NOTE sysout framecount 
		System.out.println("VSDCon addchart() framecount: "+ frameCount);
		short[] averageForRegion = new short[frameCount];
		//gets data for chart
		for (int j = 0; j < frameCount; j++) {
			VSDFrame frame = lowRes.getMainData().getFrames()[j];
			Shape scaledShape = vsdView.lowResOverlay.getScaledShape(shape);
			short[] selection = ImagingFunctions.polygonPixels(scaledShape, frame.getData(), frame.getxSize());
			averageForRegion[j] = ImagingFunctions.averagePixelValue(selection);
		}
		vsdView.analysisPanel.makeChart(averageForRegion, label, lowRes.getMainData().getFormInfo().getdSampleTime());
		vsdView.revalidate();
		vsdView.repaint();
	}
	
	// remove the chart whose title is the id
	public void removeChart(String id)
	{
		vsdView.analysisPanel.removeChart(id);
		vsdView.revalidate();
		vsdView.repaint();
	}

	/**
	 * returns the VSDData object given the path
	 * 
	 * @param path
	 * @return
	 * @throws FileNotFoundException
	 */
	public VSDData getVSDDataFromPath(String path) throws FileNotFoundException {
		GsdReader reader = new GsdReader(path);
		GsdData gsdData = reader.getGsdData();
		VSDData vsdData = new VSDData();
		vsdData.setMainData(gsdData);
		return vsdData;
	}
}
