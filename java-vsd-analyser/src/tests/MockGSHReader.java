package tests;

import vsdController.GshReader;

public class MockGSHReader extends GshReader{

	public MockGSHReader()
	{
		super();
	}
	
	public MockGSHReader(String path)
	{
		super(path);
	}
	
	public void pubDecodeAndAssign(String testString)
	{
		decodeAndAssign(testString);
	}
}