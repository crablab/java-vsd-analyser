package tests;

import static org.junit.Assert.*;
import helpers.ImagingFunctions;

import java.awt.Point;
import java.awt.Polygon;

import org.junit.Test;

public class ImagingFunctionsTests {

	@Test
	public void averagePixelsOverOneArray()
	{
		short[] pixels = new short[]{200, 2, 4, 19};
		int expectedAvg = 56;
		assertEquals(expectedAvg, ImagingFunctions.averagePixelValue(pixels));
	}
	
	@Test
	public void averagePixelsOverMultipleArrays()
	{
		short[] pixelsFrame1 = new short[]{200, 2, 4, 19}; // 56
		short[] pixelsFrame2 = new short[]{400, 30, 30, 1}; // 115.25 = 115
		short[] pixelsFrame3 = new short[]{100, 900, 2, 50}; // 263
		short[][] pixelsAllFrames = new short[][]{pixelsFrame1, pixelsFrame2, pixelsFrame3};
		int[] expectedAvgs = new int[]{56, 115, 263};
		int[] actualAvgs = ImagingFunctions.averagePixelValues(pixelsAllFrames);
		assertArrayEquals(expectedAvgs, actualAvgs);
	}
	
	@Test
	public void enlargeImage2by2Scale1Unchanged()
	{
		short[] originalPixels = new short[]{1,2,3,4};
		int width =2;
		int height = 2;
		int scale = 1;
		short[] expectedEnlarge = new short[]{1,2,3,4};
		short[] actualEnlarge = ImagingFunctions.enlargeShortImage(originalPixels, width, height, scale);
		assertArrayEquals(expectedEnlarge, actualEnlarge);
	}
	
	@Test
	public void enlargeImage2by2Scale2()
	{
		short[] originalPixels = new short[]{1,2,3,4};
		int width =2;
		int height = 2;
		int scale = 2;
		short[] expectedEnlarge = new short[]{1,1,2,2,1,1,2,2,3,3,4,4,3,3,4,4};
		short[] actualEnlarge = ImagingFunctions.enlargeShortImage(originalPixels, width, height, scale);
		assertArrayEquals(expectedEnlarge, actualEnlarge);
	}
	
	@Test
	public void enlargeImage2by2Scale3()
	{
		short[] originalPixels = new short[]{1,2,3,4};
		int width =2;
		int height = 2;
		int scale = 3;
		short[] expectedEnlarge = new short[]{1,1,1,2,2,2,1,1,1,2,2,2,1,1,1,2,2,2,3,3,3,4,4,4,3,3,3,4,4,4,3,3,3,4,4,4};
		short[] actualEnlarge = ImagingFunctions.enlargeShortImage(originalPixels, width, height, scale);
		assertArrayEquals(expectedEnlarge, actualEnlarge);
	}
	
	@Test
	public void enlargeImage3by2Scale3()
	{
		short[] originalPixels = new short[]{1,2,3,4,5,6};
		int width =3;
		int height = 2;
		int scale = 3;
		short[] expectedEnlarge = new short[]{1,1,1,2,2,2,3,3,3,1,1,1,2,2,2,3,3,3,1,1,1,2,2,2,3,3,3,4,4,4,5,5,5,6,6,6,4,4,4,5,5,5,6,6,6,4,4,4,5,5,5,6,6,6};
		short[] actualEnlarge = ImagingFunctions.enlargeShortImage(originalPixels, width, height, scale);
		assertArrayEquals(expectedEnlarge, actualEnlarge);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void enlargeImage2by2ScaleMinus1Error()
	{
		short[] originalPixels = new short[]{1,2,3,4};
		int width =2;
		int height = 2;
		int scale = -1;

		ImagingFunctions.enlargeShortImage(originalPixels, width, height, scale);
	}
	
	@Test
	public void scalePoint5Old5New10()
	{
		int value = 5;
		int oldSize = 5;
		int newSize = 10;
		int expected = 10;
		int actual = ImagingFunctions.scaleVal(value, oldSize, newSize);
		assertEquals(expected, actual);
	}
	
	@Test
	public void getPointFromIntPos6Width5()
	{
		int width = 5;
		int pos = 6;
		Point expected = new Point(1, 1);
		Point actual = ImagingFunctions.getPointFromIntPos(pos, width);
		assertEquals(expected, actual);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void getPointFromIntPos0Width0()
	{
		int width = 0;
		int pos = 0;
		ImagingFunctions.getPointFromIntPos(pos, width);
	}
	
	@Test
	public void getPointFromIntPos6Width6()
	{
		int width = 6;
		int pos = 6;
		Point expected = new Point(0, 1);
		Point actual = ImagingFunctions.getPointFromIntPos(pos, width);
		assertEquals(expected, actual);
	}
	
	public short[] initialiseArray(int size)
	{
		short[] array = new short[size];
		for (short i = 0; i < array.length; i++) {
			array[i] = i; 
		}
		return array;
	}
	
	@Test
	public void polygonPixelsConvexPolygon()
	{
		short[] image = initialiseArray(100);
		int width = 10;
		
		int[] xPoints = new int[]{5,  8, 8, 4, 2};
		int[] yPoints = new int[]{2, 3, 6, 7, 4};
		
		Polygon convexPolygon = new Polygon(xPoints, yPoints, xPoints.length);
		short[] expectedPixels = new short[]{34, 35, 36, 37, 42, 43, 44, 45, 46, 47, 53, 54, 55, 56, 57, 64, 65, 66, 67};
		short[] actualPixels = ImagingFunctions.polygonPixels(convexPolygon, image, width);
		assertArrayEquals(expectedPixels, actualPixels);
	}
	
	@Test
	public void polygonPixelsConcavePolygon()
	{
		short[] image = initialiseArray(100);
		
		int width = 10;
		int[] xPoints = new int[]{5,  8, 8, 6, 4};
		int[] yPoints = new int[]{2, 3, 6, 4, 4};
		
		Polygon convexPolygon = new Polygon(xPoints, yPoints, xPoints.length);
		short[] expectedPixels = new short[]{35, 36, 37, 46, 47, 57};
		short[] actualPixels = ImagingFunctions.polygonPixels(convexPolygon, image, width);
		assertArrayEquals(expectedPixels, actualPixels);
	}
	
	@Test (expected = IllegalArgumentException.class)
	public void polygonPixelsNotInImage()
	{
		short[] image = initialiseArray(100);
		int width = 10;
		
		int[] xPoints = new int[]{10, 20, 30, 20};
		int[] yPoints = new int[]{10, 5, 20, 15};
		Polygon outsidePolygon = new Polygon(xPoints, yPoints, xPoints.length);
		ImagingFunctions.polygonPixels(outsidePolygon, image, width);
	}
	
	@Test
	public void addPadding2and1To3x3()
	{
		short[] data = new short[]{1,2,3,4,5,6,7,8,9};
		int width = 3;
		int height = 3;
		int widthPadding = 2;
		int heightPadding = 1;
		
		short[] expectedResult = new short[]{0,0,0,0,0,0,0,0,0,1,2,3,0,0,0,0,4,5,6,0,0,0,0,7,8,9,0,0,0,0,0,0,0,0,0};
		short[] actualResult = ImagingFunctions.addPaddingToImage(data, width, height, widthPadding, heightPadding);
		assertArrayEquals(expectedResult, actualResult);
	}
	
	@Test
	public void addPadding1and2To3x3()
	{
		short[] data = new short[]{1,2,3,4,5,6,7,8,9};
		int width = 3;
		int height = 3;
		int widthPadding = 1;
		int heightPadding = 2;
		
		short[] expectedResult = new short[]{0,0,0,0,0,0,0,0,0,0,0,1,2,3,0,0,4,5,6,0,0,7,8,9,0,0,0,0,0,0,0,0,0,0,0};
		short[] actualResult = ImagingFunctions.addPaddingToImage(data, width, height, widthPadding, heightPadding);
		assertArrayEquals(expectedResult, actualResult);
	}
	
	@Test
	public void addPadding8and4To3x3()
	{
		short[] data = new short[]{1,2,3,4,5,6,7,8,9};
		int width = 3;
		int height = 3;
		int widthPadding = 4;
		int heightPadding = 2;
		
		short[] expectedResult = new short[]{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,2,3,0,0,0,0,0,0,0,0,4,5,6,0,0,0,0,0,0,0,0,7,8,9,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
		short[] actualResult = ImagingFunctions.addPaddingToImage(data, width, height, widthPadding, heightPadding);
		assertArrayEquals(expectedResult, actualResult);
	}

}