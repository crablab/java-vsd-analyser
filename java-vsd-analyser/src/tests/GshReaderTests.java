package tests;

import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;
import vsdController.GshReader;
import vsdModel.GshData;

public class GshReaderTests {

	private MockGSHReader gshObj;
	@Before
	public void SetUp()
	{
		gshObj = new MockGSHReader();
		//gshObj.initialiseFields();
	}

	@Test
	public void getSeperator_Equals_returnsEquals()
	{
		String toTest = "fieldName = fieldValue";

		assertEquals(GshReader.SeparatorEnum.EQUALS_SEP, GshReader.getSeperator(toTest));
	}
	
	@Test
	public void getSeperator_Colon_returnsColon()
	{
		String toTest = "fieldName: fieldValue";

		assertEquals(GshReader.SeparatorEnum.COLON_SEP, GshReader.getSeperator(toTest));
	}
	
	@Test
	public void getSeperator_None_returnsNA()
	{
		String toTest = "fieldName fieldValue";

		assertEquals(GshReader.SeparatorEnum.NA, GshReader.getSeperator(toTest));
	}
	
	@Test
	public void testDecodeAndAssign_ValidDateString_DateString()
	{
		String dateString = "2013/08/22 11:15:33";
		String validString = GshReader.ACQUISITION_DATE_STRING + " = " + dateString;

		gshObj.pubDecodeAndAssign(validString);
		
		assertEquals(dateString, gshObj.getFields().get(GshReader.ACQUISITION_DATE_STRING));
	}
	
	@Test
	public void testDecodeAndAssign_InValidDate_InvalidDateString()
	{
		String dateString = "20asdas/22 11:15:33";
		String validString = GshReader.ACQUISITION_DATE_STRING + " = " + dateString;

		gshObj.pubDecodeAndAssign(validString);
		
		assertEquals("20asdas/22 11:15:33", gshObj.getFields().get(GshReader.ACQUISITION_DATE_STRING));
	}
	
	@Test
	public void testDecodeAndAssign_xpx()
	{
		String xpxString = "512";
		String ypxString = "256";
		String validString = GshReader.XPX_STRING + ": " + xpxString + " " +  GshReader.YPX_STRING + ": " + ypxString;

		gshObj.pubDecodeAndAssign(validString);
		
		assertEquals("512", gshObj.getFields().get(GshReader.XPX_STRING));
	}
	
	@Test
	public void testDecodeAndAssign_ypx()
	{
		String xpxString = "512";
		String ypxString = "256";
		String validString = GshReader.XPX_STRING + ": " + xpxString +  " " + GshReader.YPX_STRING + ": " + ypxString;


		gshObj.pubDecodeAndAssign(validString);
		
		assertEquals("256", gshObj.getFields().get(GshReader.YPX_STRING));
	}
	
	@Test
	public void testAddGshData_XpxNoError_ValueAssigned()
	{
		String xpxString = "512";
		
		//the erroneous field
		String InvalidYpxString = "sd6";
		
		String InvalidString = GshReader.XPX_STRING + ": " + xpxString +  " " + GshReader.YPX_STRING + ": " + InvalidYpxString;

		gshObj.pubDecodeAndAssign(InvalidString);
		GshData gshData = gshObj.generateGshData();
		
		
		assertEquals(512, gshData.getXpx());
	}
	
	@Test
	public void testAddGshData_YpxError_ErroValue()
	{
		String xpxString = "512";
		
		//the erroneous field
		String InvalidYpxString = "sd6";
		
		String InvalidString = GshReader.XPX_STRING + ": " + xpxString +  " " + GshReader.YPX_STRING + ": " + InvalidYpxString;

		gshObj.pubDecodeAndAssign(InvalidString);
		GshData gshData = gshObj.generateGshData();

		assertEquals(-1, gshData.getYpx());
	}

}
