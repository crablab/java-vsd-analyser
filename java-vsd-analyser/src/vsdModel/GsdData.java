package vsdModel;

import java.util.List;

/**
 * The GSD data information, includes all frames of the STG (differential data)
 * @author Andy Stabler 101748285
 *
 */
public class GsdData {
	
	private String fileName;

	private String headerInformation;
	
	private DataFormatInfo formInfo;
	
	private InputSignalInfo inputSignalInfo;
	
	private ControlInfo controlInfo;
	
	private VSDFrame backgroundImage;
	
	private VSDFrame[] frames;
	
	private AnalogInputSignal analogInputSignal;
	
	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	
	public String getHeaderInformation() {
		return headerInformation;
	}

	public void setHeaderInformation(String headerInformation) {
		this.headerInformation = headerInformation;
	}

	public DataFormatInfo getFormInfo() {
		return formInfo;
	}

	public void setFormInfo(DataFormatInfo formInfo) {
		this.formInfo = formInfo;
	}

	public InputSignalInfo getInputSignalInfo() {
		return inputSignalInfo;
	}

	public void setInputSignalInfo(InputSignalInfo inputSignalInfo) {
		this.inputSignalInfo = inputSignalInfo;
	}

	public ControlInfo getControlInfo() {
		return controlInfo;
	}

	public void setControlInfo(ControlInfo controlInfo) {
		this.controlInfo = controlInfo;
	}

	public VSDFrame getBackgroundImage() {
		return backgroundImage;
	}

	public void setBackgroundImage(VSDFrame backgroundImage) {
		this.backgroundImage = backgroundImage;
	}

	public VSDFrame[] getFrames() {
		return frames;
	}

	public void setFrames(VSDFrame[] frames) {
		this.frames = frames;
	}

	public AnalogInputSignal getAnalogInputSignal() {
		return analogInputSignal;
	}

	public void setAnalogInputSignal(AnalogInputSignal analogInputSignal) {
		this.analogInputSignal = analogInputSignal;
	}
	
}
