package vsdModel;

public class ControlInfo {

	private byte[] controlData;

	/**
	 * @return the controlData
	 */
	public byte[] getControlData() {
		return controlData;
	}

	/**
	 * @param controlData the controlData to set
	 */
	public void setControlData(byte[] controlData) {
		this.controlData = controlData;
	}
	
	public static ControlInfo decodeControlInfo(byte[] byteArray)
	{
		ControlInfo cInfo = new ControlInfo();
		cInfo.setControlData(byteArray);
		return cInfo;
	}
	
	public String toString()
	{
		StringBuilder sBuilder = new StringBuilder();
		sBuilder.append("\nCONTROL DATA\n");
		for (byte data : controlData) {
			sBuilder.append(data);
		}
		return sBuilder.toString();
	}
	
}