package vsdModel;

import java.util.Date;

/**
 * 
 * The header information
 * 
 * @author Andy Stabler 101748285
 *
 */
public class GshData  {
	
	private String dataName;
	private Date aquisitionDate;
	private String dataSize;
	private String frameSizeString;
	private String samplingTime;
	private double averaging;
	private String cmt;
	
	private String Camera;
	private int xpx;
	private int ypx;
	private String channel;
	private String mirror;
	private String frames;
	private String sampling;
	private String trigger;
	private String pageMode;
	private String average;
	private String ch1;
	private String ch2;
	private String delayedStimulation;
    private String shutdly;
    private String referenceAverage;
    
    public String toString()
    {
    	StringBuilder str = new StringBuilder();
    	str.append("dataName = " + getDataName());
    	str.append("\naquiisitionDate = " + getAquisitionDate());
    	str.append("\ndataSize = " + getDataSize());
    	str.append("\nframeSize = " + getFrameSizeString());
    	str.append("\nsamplingTime = " + getSamplingTime());
    	str.append("\naveraging = " + getAveraging());
    	str.append("\ncmt = " + getCmt());
    	str.append("\nCamera = " + getCamera());
    	str.append("\nxpx = " + getXpx());
    	str.append("\nypx = " + getYpx());
    	str.append("\nchannel = " + getChannel());
    	str.append("\nmirror = " + getMirror());
    	str.append("\nframes = " + getFrames());
    	str.append("\nsampling = " + getSampling());
    	str.append("\ntrigger = " + getTrigger());
    	str.append("\npageMode = " + getPageMode());
    	str.append("\naverage = " + getAverage());
    	str.append("\nch1 = " + getCh1());
    	str.append("\nch2 = " + getCh2());
    	str.append("\nshutdly = " + getShutdly());
    	str.append("\nreferenceAverage = " + getReferenceAverage());
    	return str.toString();
    }
    
    /**
	 * @return the dataName
	 */
	public String getDataName() {
		return dataName;
	}
	
	/**
	 * @param dataName the dataName to set
	 */
	public void setDataName(String dataName) {
		this.dataName = dataName;
	}
	
	/**
	 * @return the aquisitionDate
	 */
	public Date getAquisitionDate() {
		return aquisitionDate;
	}
	
	/**
	 * @param aquisitionDate the aquisitionDate to set
	 */
	public void setAquisitionDate(Date aquisitionDate) {
		this.aquisitionDate = aquisitionDate;
	}
	
	/**
	 * @return the dataSize
	 */
	public String getDataSize() {
		return dataSize;
	}
	
	/**
	 * @param dataSize the dataSize to set
	 */
	public void setDataSize(String dataSize) {
		this.dataSize = dataSize;
	}
	/**
	 * @return the frameSizeString
	 */
	public String getFrameSizeString() {
		return frameSizeString;
	}
	/**
	 * @param frameSizeString the frameSizeString to set
	 */
	public void setFrameSizeString(String frameSizeString) {
		this.frameSizeString = frameSizeString;
	}
	/**
	 * @return the samplingTime
	 */
	public String getSamplingTime() {
		return samplingTime;
	}
	/**
	 * @param samplingTime the samplingTime to set
	 */
	public void setSamplingTime(String samplingTime) {
		this.samplingTime = samplingTime;
	}
	/**
	 * @return the averaging
	 */
	public double getAveraging() {
		return averaging;
	}
	/**
	 * @param averaging the averaging to set
	 */
	public void setAveraging(double averaging) {
		this.averaging = averaging;
	}
	/**
	 * @return the cmt
	 */
	public String getCmt() {
		return cmt;
	}
	/**
	 * @param cmt the cmt to set
	 */
	public void setCmt(String cmt) {
		this.cmt = cmt;
	}
	/**
	 * @return the camera
	 */
	public String getCamera() {
		return Camera;
	}
	/**
	 * @param camera the camera to set
	 */
	public void setCamera(String camera) {
		Camera = camera;
	}
	/**
	 * @return the xpx
	 */
	public int getXpx() {
		return xpx;
	}
	/**
	 * @param xpx the xpx to set
	 */
	public void setXpx(int xpx) {
		this.xpx = xpx;
	}
	/**
	 * @return the ypx
	 */
	public int getYpx() {
		return ypx;
	}
	/**
	 * @param ypx the ypx to set
	 */
	public void setYpx(int ypx) {
		this.ypx = ypx;
	}
	/**
	 * @return the channel
	 */
	public String getChannel() {
		return channel;
	}
	/**
	 * @param channel the channel to set
	 */
	public void setChannel(String channel) {
		this.channel = channel;
	}
	/**
	 * @return the mirror
	 */
	public String getMirror() {
		return mirror;
	}
	/**
	 * @param mirror the mirror to set
	 */
	public void setMirror(String mirror) {
		this.mirror = mirror;
	}
	/**
	 * @return the frames
	 */
	public String getFrames() {
		return frames;
	}
	/**
	 * @param frames the frames to set
	 */
	public void setFrames(String frames) {
		this.frames = frames;
	}
	/**
	 * @return the sampling
	 */
	public String getSampling() {
		return sampling;
	}
	/**
	 * @param sampling the sampling to set
	 */
	public void setSampling(String sampling) {
		this.sampling = sampling;
	}
	/**
	 * @return the trigger
	 */
	public String getTrigger() {
		return trigger;
	}
	/**
	 * @param trigger the trigger to set
	 */
	public void setTrigger(String trigger) {
		this.trigger = trigger;
	}
	/**
	 * @return the pageMode
	 */
	public String getPageMode() {
		return pageMode;
	}
	/**
	 * @param pageMode the pageMode to set
	 */
	public void setPageMode(String pageMode) {
		this.pageMode = pageMode;
	}
	/**
	 * @return the average
	 */
	public String getAverage() {
		return average;
	}
	/**
	 * @param average the average to set
	 */
	public void setAverage(String average) {
		this.average = average;
	}
	/**
	 * @return the ch1
	 */
	public String getCh1() {
		return ch1;
	}
	/**
	 * @param ch1 the ch1 to set
	 */
	public void setCh1(String ch1) {
		this.ch1 = ch1;
	}
	/**
	 * @return the ch2
	 */
	public String getCh2() {
		return ch2;
	}
	/**
	 * @param ch2 the ch2 to set
	 */
	public void setCh2(String ch2) {
		this.ch2 = ch2;
	}
	/**
	 * @return the delayedStimulation
	 */
	public String getDelayedStimulation() {
		return delayedStimulation;
	}
	/**
	 * @param delayedStimulation the delayedStimulation to set
	 */
	public void setDelayedStimulation(String delayedStimulation) {
		this.delayedStimulation = delayedStimulation;
	}
	/**
	 * @return the shutdly
	 */
	public String getShutdly() {
		return shutdly;
	}
	/**
	 * @param shutdly the shutdly to set
	 */
	public void setShutdly(String shutdly) {
		this.shutdly = shutdly;
	}
	/**
	 * @return the referenceAverage
	 */
	public String getReferenceAverage() {
		return referenceAverage;
	}
	/**
	 * @param referenceAverage the referenceAverage to set
	 */
	public void setReferenceAverage(String referenceAverage) {
		this.referenceAverage = referenceAverage;
	}

    
}
