package vsdModel;

import helpers.ByteConversions;

import org.omg.CORBA.PRIVATE_MEMBER;

public class InputSignalInfo {

	/**
	 * Number of analogue channels
	 */
	private short nChanum;
	
	/**
	 * Temporal resolution
	 */
	private short nRate;
	
	private short nOffset;
	
	private short nChNext;
	
	private short nTimeNext;
	
	/**
	 * Number of Frames
	 */
	private short nFrameSize;
	
	private short nShift;
	
	private short nDummy1;
	
	private short nDummy2;
	
	private short nDummy3;

	/**
	 * @return the nChanum
	 */
	public short getnChanum() {
		return nChanum;
	}

	/**
	 * @param nChanum the nChanum to set
	 */
	public void setnChanum(short nChanum) {
		this.nChanum = nChanum;
	}

	/**
	 * @return the nRate
	 */
	public short getnRate() {
		return nRate;
	}

	/**
	 * @param nRate the nRate to set
	 */
	public void setnRate(short nRate) {
		this.nRate = nRate;
	}

	/**
	 * @return the nOffset
	 */
	public short getnOffset() {
		return nOffset;
	}

	/**
	 * @param nOffset the nOffset to set
	 */
	public void setnOffset(short nOffset) {
		this.nOffset = nOffset;
	}

	/**
	 * @return the nChNext
	 */
	public short getnChNext() {
		return nChNext;
	}

	/**
	 * @param nChNext the nChNext to set
	 */
	public void setnChNext(short nChNext) {
		this.nChNext = nChNext;
	}

	/**
	 * @return the nTimeNext
	 */
	public short getnTimeNext() {
		return nTimeNext;
	}

	/**
	 * @param nTimeNext the nTimeNext to set
	 */
	public void setnTimeNext(short nTimeNext) {
		this.nTimeNext = nTimeNext;
	}

	/**
	 * @return the nFrameSize
	 */
	public short getnFrameSize() {
		return nFrameSize;
	}

	/**
	 * @param nFrameSize the nFrameSize to set
	 */
	public void setnFrameSize(short nFrameSize) {
		this.nFrameSize = nFrameSize;
	}

	/**
	 * @return the nShift
	 */
	public short getnShift() {
		return nShift;
	}

	/**
	 * @param nShift the nShift to set
	 */
	public void setnShift(short nShift) {
		this.nShift = nShift;
	}

	/**
	 * @return the nDummy1
	 */
	public short getnDummy1() {
		return nDummy1;
	}

	/**
	 * @param nDummy1 the nDummy1 to set
	 */
	public void setnDummy1(short nDummy1) {
		this.nDummy1 = nDummy1;
	}

	/**
	 * @return the nDummy2
	 */
	public short getnDummy2() {
		return nDummy2;
	}

	/**
	 * @param nDummy2 the nDummy2 to set
	 */
	public void setnDummy2(short nDummy2) {
		this.nDummy2 = nDummy2;
	}

	/**
	 * @return the nDummy3
	 */
	public short getnDummy3() {
		return nDummy3;
	}

	/**
	 * @param nDummy3 the nDummy3 to set
	 */
	public void setnDummy3(short nDummy3) {
		this.nDummy3 = nDummy3;
	}
	
	public static InputSignalInfo decodeInputSignalInfo(byte[] byteArray)
	{
		System.out.println("decoding input signal info...");
		
		// 20 bytes
		int auxInfoSize = 20;
		if (byteArray.length != auxInfoSize)
			throw new IllegalArgumentException("The array size was not 20 bytes");
		
		InputSignalInfo signalInfo = new InputSignalInfo();
		signalInfo.initialiseFields();
		
		int bytePos = 0;
		signalInfo.setnChanum(ByteConversions.getShortFromBytes(byteArray[bytePos++], byteArray[bytePos++]));
		signalInfo.setnRate(ByteConversions.getShortFromBytes(byteArray[bytePos++], byteArray[bytePos++]));
		signalInfo.setnOffset(ByteConversions.getShortFromBytes(byteArray[bytePos++], byteArray[bytePos++]));
		signalInfo.setnChNext(ByteConversions.getShortFromBytes(byteArray[bytePos++], byteArray[bytePos++]));
		signalInfo.setnTimeNext(ByteConversions.getShortFromBytes(byteArray[bytePos++], byteArray[bytePos++]));
		signalInfo.setnFrameSize(ByteConversions.getShortFromBytes(byteArray[bytePos++], byteArray[bytePos++]));
		signalInfo.setnShift(ByteConversions.getShortFromBytes(byteArray[bytePos++], byteArray[bytePos++]));
		signalInfo.setnDummy1(ByteConversions.getShortFromBytes(byteArray[bytePos++], byteArray[bytePos++]));
		signalInfo.setnDummy2(ByteConversions.getShortFromBytes(byteArray[bytePos++], byteArray[bytePos++]));
		signalInfo.setnDummy3(ByteConversions.getShortFromBytes(byteArray[bytePos++], byteArray[bytePos++]));
		return signalInfo;
	}
	
	private void initialiseFields()
	{
		setnChanum(Short.MIN_VALUE);
		setnRate(Short.MIN_VALUE);
		setnOffset(Short.MIN_VALUE);
		setnChNext(Short.MIN_VALUE);
		setnTimeNext(Short.MIN_VALUE);
		setnFrameSize(Short.MIN_VALUE);
		setnShift(Short.MIN_VALUE);
		setnDummy1(Short.MIN_VALUE);
		setnDummy2(Short.MIN_VALUE);
		setnDummy3(Short.MIN_VALUE);
	}

	public String toString()
	{
		StringBuilder sBuilder = new StringBuilder();
		sBuilder.append("\nINPUT SIGNAL INFO\n");
		sBuilder.append("\nnChanum = " + getnChanum());
		sBuilder.append("\nnRate = " + getnRate());
		sBuilder.append("\nnOffset = " + getnOffset());
		sBuilder.append("\nnChNext = " + getnChNext());
		sBuilder.append("\nnTimeNext = " + getnTimeNext());
		sBuilder.append("\nFrameSize = " + getnFrameSize());
		sBuilder.append("\nnShift = " + getnShift());
		sBuilder.append("\nnDummy1 = " + getnDummy1());
		sBuilder.append("\nnDummy2 = " + getnDummy2());
		sBuilder.append("\nnDUmmy3 = " + getnDummy3());
		return sBuilder.toString();
	}
	
}
