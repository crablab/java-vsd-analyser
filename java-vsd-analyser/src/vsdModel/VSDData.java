package vsdModel;

/**
 * all of the VSD data including the images and header information
 * @author Andy Stabler 101748285
 *
 */
public class VSDData {

	private GshData headerData;
	private GsdData mainData;
	
	/**
	 * @return the headerData
	 */
	public GshData getHeaderData() {
		return headerData;
	}
	/**
	 * @param headerData the headerData to set
	 */
	public void setHeaderData(GshData headerData) {
		this.headerData = headerData;
	}
	/**
	 * @return the mainData
	 */
	public GsdData getMainData() {
		return mainData;
	}
	/**
	 * @param mainData the mainData to set
	 */
	public void setMainData(GsdData mainData) {
		this.mainData = mainData;
	}

	
}
