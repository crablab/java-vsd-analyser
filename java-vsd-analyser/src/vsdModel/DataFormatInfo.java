package vsdModel;

import helpers.ByteConversions;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Arrays;

public class DataFormatInfo {

	/**
	 * Number of pixels on X axis
	 */
	private short nDataXSize;
	
	/**
	 * Number of pixels on X axis
	 */
	private short nDataYSize;

	/**
	 * Skipped number of pixels on X axis
	 */
	private short nLeftSkip;
	
	/**
	 * Skipped number of pixels on Y axis
	 */
	private short nTopSkip;
	
	/**
	 * Number of effective pixels on X axis
	 */
	private short nImgXSize;
	
	/**
	 * Number of effective pixels on Y axis
	 */
	private short nImageYSize;
	

	/**
	 * Number of frames
	 */
	private short nFrameSize;
	
	private short nOrgImgXSize;
	
	private short nOrgImgYSize;
	
	private short nOrgFrmSize;
	
	private short nShift;
	
	private short nDummy;
	
	/**
	 * Number of averaging times
	 */
	private float dAverage;
	
	/**
	 * Sampling rate (ms)
	 */
	private float dSampleTime;
	
	private float dOrgSampleTime;
	
	private float dDummy;
	
	private char chDum[];
	
	/**
	 * @return the nDataXSize
	 */
	public short getnDataXSize() {
		return nDataXSize;
	}

	/**
	 * @param nDataXSize the nDataXSize to set
	 */
	public void setnDataXSize(short nDataXSize) {
		this.nDataXSize = nDataXSize;
	}

	/**
	 * @return the nDataYSize
	 */
	public short getnDataYSize() {
		return nDataYSize;
	}

	/**
	 * @param nDataYSize the nDataYSize to set
	 */
	public void setnDataYSize(short nDataYSize) {
		this.nDataYSize = nDataYSize;
	}

	/**
	 * @return the nLeftSkip
	 */
	public short getnLeftSkip() {
		return nLeftSkip;
	}

	/**
	 * @param nLeftSkip the nLeftSkip to set
	 */
	public void setnLeftSkip(short nLeftSkip) {
		this.nLeftSkip = nLeftSkip;
	}

	/**
	 * @return the nTopSkip
	 */
	public short getnTopSkip() {
		return nTopSkip;
	}

	/**
	 * @param nTopSkip the nTopSkip to set
	 */
	public void setnTopSkip(short nTopSkip) {
		this.nTopSkip = nTopSkip;
	}

	/**
	 * @return the nImgXSize
	 */
	public short getnImgXSize() {
		return nImgXSize;
	}

	/**
	 * @param nImgXSize the nImgXSize to set
	 */
	public void setnImgXSize(short nImgXSize) {
		this.nImgXSize = nImgXSize;
	}

	/**
	 * @return the nImageYSize
	 */
	public short getnImageYSize() {
		return nImageYSize;
	}

	/**
	 * @param nImageYSize the nImageYSize to set
	 */
	public void setnImageYSize(short nImageYSize) {
		this.nImageYSize = nImageYSize;
	}

	/**
	 * @return the nFrameSize
	 */
	public short getnFrameSize() {
		return nFrameSize;
	}

	/**
	 * @param nFrameSize the nFrameSize to set
	 */
	public void setnFrameSize(short nFrameSize) {
		this.nFrameSize = nFrameSize;
	}

	/**
	 * @return the nOrgImgXSize
	 */
	public short getnOrgImgXSize() {
		return nOrgImgXSize;
	}

	/**
	 * @param nOrgImgXSize the nOrgImgXSize to set
	 */
	public void setnOrgImgXSize(short nOrgImgXSize) {
		this.nOrgImgXSize = nOrgImgXSize;
	}

	/**
	 * @return the nOrgImgYSize
	 */
	public short getnOrgImgYSize() {
		return nOrgImgYSize;
	}

	/**
	 * @param nOrgImgYSize the nOrgImgYSize to set
	 */
	public void setnOrgImgYSize(short nOrgImgYSize) {
		this.nOrgImgYSize = nOrgImgYSize;
	}

	/**
	 * @return the nOrgFrmSize
	 */
	public short getnOrgFrmSize() {
		return nOrgFrmSize;
	}

	/**
	 * @param nOrgFrmSize the nOrgFrmSize to set
	 */
	public void setnOrgFrmSize(short nOrgFrmSize) {
		this.nOrgFrmSize = nOrgFrmSize;
	}

	/**
	 * @return the nShift
	 */
	public short getnShift() {
		return nShift;
	}

	/**
	 * @param nShift the nShift to set
	 */
	public void setnShift(short nShift) {
		this.nShift = nShift;
	}

	/**
	 * @return the nDummy
	 */
	public short getnDummy() {
		return nDummy;
	}

	/**
	 * @param nDummy the nDummy to set
	 */
	public void setnDummy(short nDummy) {
		this.nDummy = nDummy;
	}

	/**
	 * @return the dAverage
	 */
	public float getdAverage() {
		return dAverage;
	}

	/**
	 * @param dAverage the dAverage to set
	 */
	public void setdAverage(float dAverage) {
		this.dAverage = dAverage;
	}

	/**
	 * @return the dSampleTime
	 */
	public float getdSampleTime() {
		return dSampleTime;
	}

	/**
	 * @param dSampleTime the dSampleTime to set
	 */
	public void setdSampleTime(float dSampleTime) {
		this.dSampleTime = dSampleTime;
	}

	/**
	 * @return the dOrgSampleTime
	 */
	public float getdOrgSampleTime() {
		return dOrgSampleTime;
	}

	/**
	 * @param dOrgSampleTime the dOrgSampleTime to set
	 */
	public void setdOrgSampleTime(float dOrgSampleTime) {
		this.dOrgSampleTime = dOrgSampleTime;
	}

	/**
	 * @return the dDummy
	 */
	public float getdDummy() {
		return dDummy;
	}
	
	/**
	 * @param dDummy the dDummy to set
	 */
	public void setdDummy(float dDummy) {
		this.dDummy = dDummy;
	}

	/**
	 * @return the chDum
	 */
	public char[] getChDum() {
		return chDum;
	}

	/**
	 * @param chDum the chDum to set
	 */
	public void setChDum(char[] chDum) {
		this.chDum = chDum;
	}
	
	/**
	 *  decodes the byte array and creates a DataFormatInfo object from it
	 * @param byteArray
	 * @return
	 * @throws IllegalArgumentException if the byte array is not valid
	 */
	public static DataFormatInfo decodeFormatInfo(byte[] byteArray)
	{
		System.out.println("decoding format info . . .");
		int formInfoSize = 72;
		if (byteArray.length != formInfoSize)
			throw new IllegalArgumentException("The array size was not 72 bytes");
		
		DataFormatInfo dFormatInfo = new DataFormatInfo();
		dFormatInfo.initialiseFields();
		
		int bytePos = 0;
		dFormatInfo.setnDataXSize(ByteConversions.getShortFromBytes(byteArray[bytePos++], byteArray[bytePos++]));
		dFormatInfo.setnDataYSize(ByteConversions.getShortFromBytes(byteArray[bytePos++], byteArray[bytePos++]));
		dFormatInfo.setnLeftSkip(ByteConversions.getShortFromBytes(byteArray[bytePos++], byteArray[bytePos++]));
		dFormatInfo.setnTopSkip(ByteConversions.getShortFromBytes(byteArray[bytePos++], byteArray[bytePos++]));
		dFormatInfo.setnImgXSize(ByteConversions.getShortFromBytes(byteArray[bytePos++], byteArray[bytePos++]));
		dFormatInfo.setnImageYSize(ByteConversions.getShortFromBytes(byteArray[bytePos++], byteArray[bytePos++]));
		dFormatInfo.setnFrameSize(ByteConversions.getShortFromBytes(byteArray[bytePos++], byteArray[bytePos++]));
		dFormatInfo.setnOrgImgXSize(ByteConversions.getShortFromBytes(byteArray[bytePos++], byteArray[bytePos++]));
		dFormatInfo.setnOrgImgYSize(ByteConversions.getShortFromBytes(byteArray[bytePos++], byteArray[bytePos++]));
		dFormatInfo.setnOrgFrmSize(ByteConversions.getShortFromBytes(byteArray[bytePos++], byteArray[bytePos++]));
		dFormatInfo.setnShift(ByteConversions.getShortFromBytes(byteArray[bytePos++], byteArray[bytePos++]));
		dFormatInfo.setnDummy(ByteConversions.getShortFromBytes(byteArray[bytePos++], byteArray[bytePos++]));
		dFormatInfo.setdAverage(ByteConversions.getFloatFromBytes(byteArray[bytePos++], byteArray[bytePos++], byteArray[bytePos++], byteArray[bytePos++]));
		dFormatInfo.setdSampleTime(ByteConversions.getFloatFromBytes(byteArray[bytePos++], byteArray[bytePos++], byteArray[bytePos++], byteArray[bytePos++]));
		dFormatInfo.setdOrgSampleTime(ByteConversions.getFloatFromBytes(byteArray[bytePos++], byteArray[bytePos++], byteArray[bytePos++], byteArray[bytePos++]));
		dFormatInfo.setdDummy(ByteConversions.getFloatFromBytes(byteArray[bytePos++], byteArray[bytePos++], byteArray[bytePos++], byteArray[bytePos++]));
		char[] tempChDum = ByteConversions.getCharArrayFromByteArray(Arrays.copyOfRange(byteArray, bytePos, bytePos + 32));
		dFormatInfo.setChDum(tempChDum.length <= 32 ? tempChDum : new char[32]);
	
		//System.out.println(dFormatInfo.toString());
		
		return dFormatInfo;
	}
	
	private void initialiseFields() {
		setnDataXSize(Short.MIN_VALUE);
		setnDataYSize(Short.MIN_VALUE);
		setnLeftSkip(Short.MIN_VALUE); 
		setnTopSkip(Short.MIN_VALUE);
		setnImgXSize(Short.MIN_VALUE);
		setnImageYSize(Short.MIN_VALUE);
		setnFrameSize(Short.MIN_VALUE);
		setnOrgImgXSize(Short.MIN_VALUE);
		setnOrgImgYSize(Short.MIN_VALUE);
		setnOrgFrmSize(Short.MIN_VALUE); 
		setnShift(Short.MIN_VALUE);
		setnDummy(Short.MIN_VALUE);
		setdAverage(Float.MIN_VALUE);
		setdSampleTime(Float.MIN_VALUE); 
		setdOrgSampleTime(Float.MIN_VALUE);
		setnDummy(Short.MIN_VALUE);
		chDum = new char[32];
	}
	
	public String toString()
	{
		StringBuilder sBuilder = new StringBuilder();
		sBuilder.append("\nDATA FORMAT INFO\n");
		sBuilder.append("\nnDataxSize = " + getnDataXSize());
		sBuilder.append("\nnDataYSize = " + getnDataYSize());
		sBuilder.append("\nnLeftSkip = " + getnLeftSkip());
		sBuilder.append("\nnTopSkip = " + getnTopSkip());
		sBuilder.append("\nnImgXSize = " + getnImgXSize());
		sBuilder.append("\nnImgYSize = " + getnImageYSize());
		sBuilder.append("\nnFrameSize = " + getnFrameSize());
		sBuilder.append("\nnOrgImgXSize = " + getnOrgImgXSize());
		sBuilder.append("\nnOrgImgYSize = " + getnOrgImgYSize());
		sBuilder.append("\nnOrgFrmSize = " + getnOrgFrmSize());
		sBuilder.append("\nnShift = " + getnShift());
		sBuilder.append("\nnDummy = " + getnDummy());
		sBuilder.append("\ndAverage = " + getdAverage());
		sBuilder.append("\ndSamplingTime = " + getdSampleTime());
		sBuilder.append("\ndOrgSampleTime = " + getdOrgSampleTime());
		sBuilder.append("\ndDummy = " + getdDummy());
		sBuilder.append("\nchDum = " + new String(getChDum()));
		return sBuilder.toString();
	}
}

