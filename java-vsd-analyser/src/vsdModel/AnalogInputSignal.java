package vsdModel;

import java.util.Arrays;

import helpers.ByteConversions;

/**
 * class representing the AnalogInputSignal
 *
 */
public class AnalogInputSignal {

	/**
	 * nChanum*nFarmeSize*nRate*2 bytes
	 */
	private short[] signalData;
	
	/**
	 * @return the signalData
	 */
	public short[] getSignalData() {
		return signalData;
	}

	/**
	 * @param signalData the signalData to set
	 */
	public void setSignalData(short[] signalData) {
		this.signalData = signalData;
	}

	public String toString(){
		StringBuilder sBuilder = new StringBuilder();
		sBuilder.append("\nANALOG INPUT SIGNAL DATA\n");
		for (short data : signalData) {
			sBuilder.append("\n" + data);
			
		}
		return sBuilder.toString();
	}
	
	/**
	 * converts an array of bytes into an AnalogInputSignal object
	 * @param byteArray the byte array containing the data to decode
	 * @param size the number of bytes to decode
	 * @return the AnalogInputSignal object
	 */
	public static AnalogInputSignal decode(byte[] byteArray, int size)
	{
		/*
		 * NOTE: there are two bytes in a short, so we check there are 
		 * at least 2 bytes in the array, and we also initialise the short array to
		 * half the value of size
		 */
		System.out.println("decoding analog input signal info...");

		if (byteArray.length != size || size < 2)
			throw new IllegalArgumentException("The array size was not " + size + " bytes");
		
		AnalogInputSignal signalInfo = new AnalogInputSignal();
		signalInfo.initialiseFields(size);
		
		int bytePos = 0;
		//why is this size/2
		short[] inData = new short[size/2];
		for (int i = 0; i < size/2; i++) {	
			inData[i] = ByteConversions.getShortFromBytes(byteArray[bytePos++], byteArray[bytePos++]);
		}
		inData = removeRunningZeros(inData);
		signalInfo.setSignalData(inData);
		//System.out.println(signalInfo);
		return signalInfo;
	}
	
	public static short[] removeRunningZeros(short[] data)
	{
		int lastPos = data.length - 1;
		while(lastPos > 0 && data[lastPos] ==0)
			lastPos--;
		return Arrays.copyOf(data, lastPos + 1);
	}

	private void initialiseFields(int size) {
		setSignalData(new short[size]);
	}
}
