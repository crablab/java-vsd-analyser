package vsdModel;

import helpers.ImagingFunctions;

import java.util.Arrays;

/**
 * used to calculate the differential data from original image
 * @author Andy
 * <p/>
 * Expanded by Benjamin Kidd
 *
 */
public class DifferentialData {
	
	public static final String Darker = "Plus";
	public static final String Brightness = "Minus";
	private short[] dValues;
	
	/**
	 * @return the dValues
	 */
	public short[] getdValues() {
		return dValues;
	}

	/**
	 * @param dValues the dValues to set
	 */
	public void setdValues(short[] dValues) {
		this.dValues = dValues;
	}

	private VSDFrame difImageData;

	/**
	 * @return the difImageData
	 */
	public VSDFrame getDifImageData() {
		return difImageData;
	}

	/**
	 * @param difImageData the difImageData to set
	 */
	public void setDifImageData(VSDFrame difImageData) {
		this.difImageData = difImageData;
	}
	
	/**
	 * 
	 * @param data the original frame to compare the dvalues with - usually the background image/first frame
	 * @param dValues the difference values in shorts
	 */
	public DifferentialData(VSDFrame data, short[] dValues)
	{
		//.clone()'s only working because it's an array of primitive types
		// remember it's a shallow copy. use Arrays.CopyOf(array, length) for deep
		//this.dValues = Arrays.copyOf(data.getData(), data.getData().length);
		this.dValues = dValues.clone();
		difImageData = adjustImage(data, this.dValues);
	}
	
	public static VSDFrame adjustImage(VSDFrame originalImage, short[] dValues)
	{
		if(dValues.length != originalImage.getData().length)
			throw new IllegalStateException("Two array values differ in length . . .");

		short[] updatedImage = Arrays.copyOf(originalImage.getData(), originalImage.getData().length);
		
		// add the difference values to the copy of the original image to get the new frame
		for (int i = 0; i < dValues.length; i++) {
			if (dValues[i] != 0){
				updatedImage[i] = (short) (updatedImage[i] + dValues[i]);
			}
		}
		
		int widthPadding = 4;
		int heightPadding = 2;
		updatedImage = ImagingFunctions.addPaddingToImage(updatedImage, originalImage.getxSize(), originalImage.getySize(), widthPadding, heightPadding);
		VSDFrame newImg = new VSDFrame();
		newImg.setData(updatedImage);
		newImg.setxSize(originalImage.getxSize() + widthPadding * 2);
		newImg.setySize(originalImage.getySize() + heightPadding * 2);
		return newImg;
	}
	
	public String toString()
	{
		StringBuilder sBuilder = new StringBuilder();
		sBuilder.append("\nDIFFERENTIAL DATA\n");
		sBuilder.append("\nDarker: " + Darker);
		sBuilder.append("\nBrightness: " + Brightness);
		sBuilder.append(getDifImageData().toString());
		return sBuilder.toString();
	}
	

}