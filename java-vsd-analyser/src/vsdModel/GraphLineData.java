package vsdModel;

import java.awt.Point;
import java.awt.Rectangle;
import java.awt.Shape;

import javax.swing.JComponent;
/**
 * 
 * @author Benjamin Kidd
 *
 */
public class GraphLineData {
	private Shape line;
	private Rectangle labelBox;
	private String	label;
	private Point xPoint;
	public GraphLineData(){
		
	}
	GraphLineData(Shape line, Rectangle labelBox,String label){
		this.setLine(line);
		this.setLabelBox(labelBox);
		this.setLabel(label);
		
	}

	public Rectangle getLabelBox() {
		return labelBox;
	}
	public void setLabelBox(Rectangle labelBox) {
		this.labelBox = labelBox;
	}
	public Shape getLine() {
		return line;
	}
	public void setLine(Shape line) {
		this.line = line;
	}
	public String getLabel() {
		return label;
	}
	public void setLabel(String label) {
		this.label = label;
	}
	public Point getXPoint() {
		return xPoint;
	}
	public void setXPoint(Point xPoint) {
		this.xPoint =xPoint;
		
	}

}
