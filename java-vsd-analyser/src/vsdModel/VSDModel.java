package vsdModel;

/**
 * model in the system
 * @author Andy
 *
 */
public class VSDModel {

	private GsdData lowResGsd;
	private GsdData highResGsd;
	
	/**
	 * @return the lowResGsd
	 */
	public GsdData getLowResGsd() {
		return lowResGsd;
	}

	/**
	 * @param lowResGsd the lowResGsd to set
	 */
	public void setLowResGsd(GsdData lowResGsd) {
		this.lowResGsd = lowResGsd;
	}

	/**
	 * @return the highResGsd
	 */
	public GsdData getHighResGsd() {
		return highResGsd;
	}

	/**
	 * @param highResGsd the highResGsd to set
	 */
	public void setHighResGsd(GsdData highResGsd) {
		this.highResGsd = highResGsd;
	}

	// not sure about header files?
	
	public VSDModel(GsdData low, GsdData high)
	{
		lowResGsd = low;
		highResGsd = high;
	}
	
	
}
