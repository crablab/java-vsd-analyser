package vsdModel;

public class BackgroundImageData {
	public static final String Dark = "0";
	public static final int Saturation = 16383;
	
	private VSDFrame backgroundImage;

	/**
	 * @return the backgroundImageData
	 */
	public VSDFrame getBackgroundImage() {
		return backgroundImage;
	}

	/**
	 * @param backgroundImageData the backgroundImageData to set
	 */
	public void setBackgroundImage(VSDFrame backgroundImage) {
		this.backgroundImage = backgroundImage;
	}
	
	public BackgroundImageData(VSDFrame data)
	{
		setBackgroundImage(data);
	}
}
