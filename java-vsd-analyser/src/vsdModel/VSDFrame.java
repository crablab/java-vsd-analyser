package vsdModel;

import helpers.ByteConversions;

public class VSDFrame {

	public static final String Dark = "0";
	public static final int Saturation = 16383;
	
	private int xSize;
	private int ySize;
	
	/**
	 * x*y
	 */
	private short[] data;

	/**
	 * @return the data
	 */
	public short[] getData() {
		return data;
	}

	/**
	 * @param data the data to set
	 */
	public void setData(short[] data) {
		this.data = data;
	}

	/**
	 * @return the xSize
	 */
	public int getxSize() {
		return xSize;
	}

	/**
	 * @param xSize the xSize to set
	 */
	public void setxSize(int xSize) {
		this.xSize = xSize;
	}

	/**
	 * @return the ySize
	 */
	public int getySize() {
		return ySize;
	}

	/**
	 * @param ySize the ySize to set
	 */
	public void setySize(int ySize) {
		this.ySize = ySize;
	}

	/**
	 * @return the size
	 */
	public int getSize() {
		return getxSize() * getySize();
	}

	public static VSDFrame extractDifferenceFrame(byte[] byteArray, int xSize, int ySize)
	{
		int backgroundSize = xSize * ySize * 2; // 16 bit signed short 
		if (byteArray.length != backgroundSize)
			throw new IllegalArgumentException("The array size was not " + backgroundSize + " bytes");
		
		VSDFrame imgData = new VSDFrame();
		imgData.setxSize(xSize);
		imgData.setySize(ySize);
		imgData.initialiseFields();
		
		int bytePos = 0;
		short[] data = new short[xSize * ySize];
		for (int i = 0; i < xSize * ySize; i++) {
			data[i] = ByteConversions.getShortFromBytes(byteArray[bytePos++], byteArray[bytePos++]);
		}

		imgData.setData(data);
		return imgData;
	}
	
	private void initialiseFields()
	{
		data = new short[getxSize() * getySize()];
		for(int i = 0; i < getxSize() * getySize(); i++){
			data[i] = Short.MIN_VALUE; 
		}
	}
	
	public String toString()
	{
		StringBuilder sBuilder = new StringBuilder();
		sBuilder.append("\nIMAGE DATA\n");
		int colCount = 0;
		for (int i = 0; i < getxSize() * getySize(); i++) {
			
			sBuilder.append(data[i] + " ");
			
			if (colCount == getxSize())
			{
				sBuilder.append("\n");
				colCount = 0;
			}
			else {
				colCount++;
			}
		}
		
		return sBuilder.toString();
	}
	
}
