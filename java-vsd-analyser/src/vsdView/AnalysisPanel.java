package vsdView;

import javax.swing.*;

import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * contains all of the chart panels
 * 
 * Author Andy Stabler, Newcastle University
 */
public class AnalysisPanel extends JPanel implements MouseListener,
		MouseMotionListener {

	/*
	 * this is used for the synchronising zoom
	 * 
	 * the initial zoomer is the chart one the event was fired on if the initial
	 * zoomer is not null, then you know the zoom event came from a different
	 * chart
	 */
	// private VSDChartPanel initialZoomer;
	private List<AnalysisChart> charts;
	public static final String ANALOG_NAME = "Analog Input Signal";
	public static final String ID_PREFIX = "Region ";

	/** @return the charts */
	public List<AnalysisChart> getCharts() {
		return charts;
	}

	public void addChart(AnalysisChart chart) {
		if (chart == null)
			return;
		charts.add(chart);
		add(chart.getMainChartContainer());
		chart.getChart().addMouseListener(this);
		chart.getChart().addMouseMotionListener(this);
		revalidate();
		repaint();
	}

	public void removeChart(AnalysisChart chart) {
		if (chart == null) {
			return;
		}
		charts.remove(chart);
		remove(chart.getMainChartContainer());
		revalidate();
	}

	public void removeChart(String id) {
		for (int i = 0; i < charts.size(); i++) {
			String chartName = charts.get(i).getChartPanel().getName();
			if (chartName != null && chartName.equals(ID_PREFIX + id)) {
				removeChart(charts.get(i));
			}
		}
	}

	public AnalysisPanel() {
		charts = new ArrayList<>();
		setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		setOpaque(true);
	}
//NOTE this ? 50/50 I think it just uses the screen or frame x and y... try and get it to use the chart
	public void setAxisTraceForAllCharts(Point point) {
		boolean pointValid = false;
		for (AnalysisChart chart : charts) {
			if (chart.getMainChartContainer().contains(point))
				pointValid = true;
		}
		if (!pointValid)
			return;
		for (AnalysisChart chart : charts) {
			MouseEvent me = new MouseEvent(this, MouseEvent.MOUSE_CLICKED,
					Calendar.getInstance().getTimeInMillis(), 0, point.x,
					point.y, 1, false);
			chart.getChart().mouseClicked(me);
			//chart.setxPoint(point);
		}
	}

	public void makeChart(short[] values, String label, double samplingTime) {
		double sampTime = samplingTime;
		//fixes scale for first chart
		double a = sampTime;
		if(values.length > 32760){a =(float) ((sampTime *32758.5)/656878.5);}
		
		
		
		AnalysisChart chart = new AnalysisChart(label, values, a);
		chart.getMainChartContainer().setMaximumSize(new Dimension(1000, 300));
		addChart(chart);
	}

	public void clearCharts(boolean clearAnalog) {
		for (int i = charts.size() - 1; i >= 0; i--) {
			AnalysisChart currentChart = charts.get(i);
			if (clearAnalog
					|| !currentChart.getChartLabel().equals(ANALOG_NAME)) {
				removeChart(charts.get(i));
			}
		}
		revalidate();
		repaint();
	}

	public void videoTick(int iterationCount) {
		for (AnalysisChart chart : charts) {
			chart.getChart().setPlaying(true);
			chart.getChart().setTime(iterationCount);
		}
	}

	public void videoStop() {
		for (AnalysisChart chart : charts) {
			chart.getChart().setPlaying(false);
		}
	}

	@Override
	public void mouseClicked(MouseEvent e) {
		for (AnalysisChart chart : charts) {
			chart.setxPoint(e.getPoint());
		}
		
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		for (AnalysisChart chartPanel : charts) {
			chartPanel.getChart().mouseEntered(e);
		}
	}

	@Override
	public void mouseExited(MouseEvent e) {
		for (AnalysisChart chartPanel : charts) {
			chartPanel.getChart().mouseExited(e);
		}
	}

	@Override
	public void mousePressed(MouseEvent e) {
		for (AnalysisChart chartPanel : charts) {
			chartPanel.getChart().mousePressed(e);
		}
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		for (AnalysisChart chartPanel : charts) {
			chartPanel.getChart().mouseReleased(e);
		}
	}
//TODO FIX ZOOM
	@Override
	public void mouseDragged(MouseEvent e) {

		VSDChartPanel callingChart = (VSDChartPanel) e.getSource();
		// the source was did not have zoom synched? do nothing
		if (!callingChart.isZoomSync())
			return;

		for (AnalysisChart chart : charts) {
			VSDChartPanel currentChart = chart.getChart();

			// make sure we're not going to call the event twice on same chart
			// and that the current chart is set to sync zoom
			if (currentChart != callingChart && currentChart.isZoomSync()) {
				// boolean rangeZoom = currentChart.isRangeZoomable();
				// currentChart.setRangeZoomable(false);
				// currentChart.setRangeZoomable(rangeZoom);
				currentChart.mouseDragged(e);

			}
		}
	}

	@Override
	public void mouseMoved(MouseEvent e) {
		for (AnalysisChart chartPanel : charts) {
			chartPanel.getChart().mouseMoved(e);
		}
	}

	public void repaint(Graphics g) {
		super.paint(g);

	}

}
