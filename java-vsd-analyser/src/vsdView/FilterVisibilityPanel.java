package vsdView;

import org.jfree.chart.event.ChartChangeEvent;
import org.jfree.chart.event.ChartChangeListener;
import org.jfree.data.general.DatasetChangeEvent;
import org.jfree.data.general.DatasetChangeListener;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;

import javax.swing.*;
import javax.swing.border.TitledBorder;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Filter;

/**
 * Created by Andy on 07/04/2014.
 * Panel the user can use to alter the visibilty of the filters applied to a chart
 */
public class FilterVisibilityPanel extends JPanel {

    private static final String TITLE = "Filters";
    private TitledBorder titledBorder;
    private List<JCheckBox> filters;
    private VSDChartPanel chart;

    public FilterVisibilityPanel(VSDChartPanel chart)
    {
        this.chart = chart;
        chart.getXYSeriesCollection().addChangeListener(new DatasetChangeListener() {
            @Override
            public void datasetChanged(DatasetChangeEvent datasetChangeEvent) {
                update();
            }
        });
        makeComponents();
        makePanel();
    }

    private void makeComponents() {
        titledBorder = new TitledBorder(TITLE);
        filters = new ArrayList<>();
    }

    private void makePanel() {
        setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
        setBorder(titledBorder);
    }

    public void addFilter(String id)
    {
        if (checkboxExists(id)){
            removeFilter(id);
        }
        // what about non-unique IDs?
        final JCheckBox newFilter = new JCheckBox(id);

        newFilter.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                setSeriesVisibilityForCheckBox(newFilter);
            }
        });
        filters.add(newFilter);
        add(newFilter);
        newFilter.setSelected(true);
        setSeriesVisibilityForCheckBox(newFilter);
        repaint();
    }

    private void setSeriesVisibilityForCheckBox(JCheckBox checkbox)
    {
        if (checkbox == null || filters.indexOf(checkbox) == -1) throw new IllegalStateException("checkbox was null, or it wasn't in the filters list");
        // check checkbox exists
        chart.getChart().getXYPlot().getRenderer().setSeriesVisible(filters.indexOf(checkbox), checkbox.isSelected());
        chart.revalidate();
        chart.repaint();
    }

    /**
     * used to remove a filter from the filters list
     * @param id
     */
    public void removeFilter(String id)
    {
        JCheckBox filter = getCheckBox(id);
        if (filter == null) {
            System.out.println("CAN''T FIND THE FILTER TO REMOVE IT");
            return;
        }

        filters.remove(filter);
        remove(filter);
        revalidate();
        repaint();
    }

    public JCheckBox getCheckBox(String id)
    {
        for (JCheckBox cb : filters)
        {
            if (cb.getText().equals(id)) return cb;
        }
        return null;
    }

    public boolean checkboxExists(String id)
    {
        return getCheckBox(id) != null;
    }

    public void update()
    {
        filters = new ArrayList<>();
        removeAll();

        XYSeriesCollection seriesCollection = chart.getXYSeriesCollection();
        for (int i = 0; i < seriesCollection.getSeriesCount(); i++) {
            addFilter(seriesCollection.getSeries(i).getKey().toString());
        }
        revalidate();
        repaint();
    }


}
