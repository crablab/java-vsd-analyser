package vsdView;

import java.awt.Shape;

/**
 * A region the user can select on an image - usually a neuron
 */
public class Region   {
	
	private String ID;
	public String getID()
	{
		return ID;
	}
	public void setID(String label)
	{
		this.ID = label;
	}
	private Shape shape;
	public void setShape(Shape shape)
	{
		this.shape = shape;
	}
	public Shape getShape()
	{
		return shape;
	}
}
