package vsdView;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;
import java.awt.event.ItemListener;

/**
 * Created by andy on 04/05/2014.
 * Expanded by Benjamin Kidd
 */
public class SelectionTools extends JPanel {

    public JToggleButton polygonSelectionbtn;
    public JToggleButton shapeRemovalbtn;
    private JButton clearAllSelectionsbtn;
    public boolean selectionEnabled;
    

    public SelectionTools()
    {
        makeComponents();
        addComponents();
    }

    public void makeComponents()
    {
        

        polygonSelectionbtn = new JToggleButton();
        polygonSelectionbtn.setPreferredSize(new Dimension(40, 40));
        ImageIcon polygonIcon = new ImageIcon(getClass().getResource("/resources/polygonSelection.png"));
        polygonSelectionbtn.setIcon(polygonIcon);
        polygonSelectionbtn.setToolTipText("Polygon Selection");

        shapeRemovalbtn = new JToggleButton();
        shapeRemovalbtn.setPreferredSize(new Dimension(40, 40));
        ImageIcon eraserIcon = new ImageIcon(getClass().getResource("/resources/eraser.png"));
        shapeRemovalbtn.setIcon(eraserIcon);
        shapeRemovalbtn.setToolTipText("Region eraser");

        clearAllSelectionsbtn = new JButton();
        clearAllSelectionsbtn.setPreferredSize(new Dimension(40, 40));
        ImageIcon clearAllIcon = new ImageIcon(getClass().getResource("/resources/clearAll.png"));
        clearAllSelectionsbtn.setIcon(clearAllIcon);
        clearAllSelectionsbtn.setToolTipText("Clear all selections");
        selectionEnabled = false;
    }

    public void addComponents()
    {
        setBorder(BorderFactory.createTitledBorder("Region Selection"));
        add(polygonSelectionbtn);
        add(shapeRemovalbtn);
        add(clearAllSelectionsbtn);

    }

    public void addPolygonSelectionListener(ActionListener listener) {
        polygonSelectionbtn.addActionListener(listener);
    }

    public void addShapeRemoveListener(ActionListener listener) {
        shapeRemovalbtn.addActionListener(listener);
    }

    public void addClearAllSelectionsListener(ActionListener listener) {
        clearAllSelectionsbtn.addActionListener(listener);
    }



}
