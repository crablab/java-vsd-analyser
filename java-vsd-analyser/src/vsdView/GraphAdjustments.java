package vsdView;

import java.awt.event.ItemListener;

import javax.swing.BorderFactory;
import javax.swing.JCheckBox;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class GraphAdjustments extends JPanel{
    private JCheckBox autoDeTrend;
    private static JTextField detrendInput;
    private JCheckBox autoSmooth;
    private static JTextField smoothInput;
    private JCheckBox autoNormalize;

    
    
    public GraphAdjustments()
    {
    	setBorder(BorderFactory.createTitledBorder("Graph Adjustments"));
        makeComponents();
        addComponents();
    }


	private void addComponents() {
        add(autoDeTrend);
        add(detrendInput);
        add(autoSmooth);
        add(smoothInput);
        add(autoNormalize);
		
	}


	private void makeComponents() {
		autoDeTrend = new JCheckBox("Auto Detrend");
        detrendInput = new JTextField("20",3);
        
        autoSmooth = new JCheckBox("Auto Smoothing");
        smoothInput = new JTextField("20",3);
        
        autoNormalize = new JCheckBox("Auto Normalize");
       
		
	}
    public void addAutoDetrendListener(ItemListener listener) {
        autoDeTrend.addItemListener(listener);
        autoDeTrend.setSelected(true);
    }
    public void addAutoSmoothListener(ItemListener listener) {
        autoSmooth.addItemListener(listener);
        autoSmooth.setSelected(false);
    }
    public void addAutoNormalizeListener(ItemListener listener) {
        autoNormalize.addItemListener(listener);
        autoNormalize.setSelected(false);
    }
    public static int getSmoothWindow(){
    	return Integer.parseInt(smoothInput.getText());
    	
    }


	public static int getDetrendWindow() {
		return Integer.parseInt(detrendInput.getText());
	}

}
