package vsdView;

import helpers.ImagingFunctions;
import vsdController.VSDController;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.geom.Line2D;
import java.awt.geom.Point2D;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.List;

public class Overlay extends JPanel {

	private int width, height;
	private boolean enabled;
	private boolean drawingLine;
	private Point2D currentLineStart;
	private Point2D currentPolygonStart;
	private List<Region> regions;
	private List<Line2D> currentSelection;
	// this Line will update as the user moves the mouse to select a new region
	private Line2D currentLine;
	private boolean erasing;
	private Point2D currentPoint;

	private BufferedImage selectionsImage;
	private LowResOverlay overlay;
	private int drawingWidth;
	
	private VSDController controller;
	
	public void setController(VSDController controller)
	{
		this.controller = controller;
	}
	
	public VSDController getController()
	{
		return controller;
	}
	
	public void setCurrentSelection(List<Line2D> selection)
	{
		currentSelection = selection;
		if (overlay != null)
			overlay.setCurrentSelection(selection);
		paintShapesToImage();
	}
	
	/** @return the enabled */
	public boolean isEnabled() {
		return enabled;
	}

	/** @param enabled the enabled to set */
	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
		if (overlay != null && enabled)
			overlay.setPointerColor(Color.GREEN);
			
	}

	/** @return the drawingLine */
	public boolean isDrawingLine() {
		return drawingLine;
	}

	/** @param drawingLine the drawingLine to set */
	public void setDrawingLine(boolean drawingLine) {
		this.drawingLine = drawingLine;
		if (overlay != null)
			overlay.setDrawingLine(drawingLine);
	}

	/** @return the currentLineStart */
	public Point2D getCurrentLineStart() {
		return currentLineStart;
	}

	/** @param currentLineStart the currentLineStart to set */
	public void setCurrentLineStart(Point2D currentLineStart) {
		this.currentLineStart = currentLineStart;
		if (overlay != null)
			overlay.setCurrentLineStart(currentLineStart);
		paintShapesToImage();
	}

	/** @return the currentPolygonStart */
	public Point2D getCurrentPolygonStart() {
		return currentPolygonStart;
	}

	/** @param currentPolygonStart the currentPolygonStart to set */
	public void setCurrentPolygonStart(Point2D currentPolygonStart) {
		this.currentPolygonStart = currentPolygonStart;
		if (overlay != null)
			overlay.setCurrentPolygonStart(currentPolygonStart);
	}

	/** @return the regions */
	public List<Region> getRegions() {
		return regions;
	}

    public Region getRegion(String id)
    {
        for (Region region : regions)
        {
            if (region.getID().equals(id))
            {
                return region;
            }
        }
        return null;
    }

	/**
	 * if we're calling this method in the superclass Overlay then we might
	 * want to generate the graphs. We won't if we're in the low-res overlay subclass
	 * @param regions
	 * @param updateGraphs
	 */
	public void setRegions(List<Region> regions, boolean updateGraphs)
	{
		this.regions = regions;
		if (overlay != null)
			overlay.setRegions(regions, updateGraphs);
	}
	
	public void addRegion(Region region)
	{
		regions.add(region);
		getController().addChart(region.getShape(), "Region " + region.getID());
	}
	
	public void removeRegion(Region region)
	{
		if (regions.contains(region))
		{
			regions.remove(region);
			System.out.println("removed shape!");
			revalidate();
			paintShapesToImage();
			getController().removeChart(region.getID());
		}

		
	}
	
	/** @return the currentSelection */
	public List<Line2D> getCurrentSelection() {
		return currentSelection;
	}

	/** @return the currentLine */
	public Line2D getCurrentLine() {
		return currentLine;
	}

	/** @param currentLine the currentLine to set */
	public void setCurrentLine(Line2D currentLine) {
		this.currentLine = currentLine;
		if (overlay != null)
			overlay.setCurrentLine(currentLine);
		paintShapesToImage();
	}
	
	/** @return the erasing */
	public boolean isErasing() {
		return erasing;
	}

	/** @param erasing the erasing to set */
	public void setErasing(boolean erasing) {
		this.erasing = erasing;
		if (overlay != null && erasing)
			overlay.setErasing(erasing);
	}

	/** @return the erasePoint	 */
	public Point2D getCurrentPoint() {
		return currentPoint;
	}

	/**	 @param currentPoint the erasePoint to set	 */
	public void setCurrentPoint(Point2D currentPoint) {
		this.currentPoint = currentPoint;
		if (overlay != null)
			overlay.setCurrentPoint(currentPoint);
		paintShapesToImage();
	}
	
	/**	 @return the overlayBufferedImage	 */
	public BufferedImage getOverlayBufferedImage() {
		return selectionsImage;
	}

	/**	 @param overlayBufferedImage the overlayBufferedImage to set	 */
	public void setOverlayBufferedImage(BufferedImage overlayBufferedImage) {
		this.selectionsImage = overlayBufferedImage;
	}

	/** @return the lowResOverlay */
	public Overlay getLowResOverlay() {
		return overlay;
	}

	/** @param overlay the lowResOverlay to set */
	public void setLowResOverlay(LowResOverlay overlay) {
		this.overlay = overlay;
		overlay.setRegions(regions, false);
		overlay.setCurrentSelection(currentSelection);
		overlay.setCurrentPoint(currentPoint);
		overlay.setCurrentLine(currentLine);
		overlay.setCurrentLineStart(currentLineStart);
		overlay.setDrawingLine(drawingLine);
		overlay.setErasing(erasing);
	}

	public int getDrawingWidth() {
		return drawingWidth;
	}

	public void setDrawingWidth(int drawingWidth) {
		this.drawingWidth = drawingWidth;
	}
	
	/**
	 * @return the selectionsImage
	 */
	public BufferedImage getSelectionsImage() {
		return selectionsImage;
	}

	/**
	 * @param selectionsImage the selectionsImage to set
	 */
	public void setSelectionsImage(BufferedImage selectionsImage) {
		this.selectionsImage = selectionsImage;
	}
	
	public Overlay(int width, int height)
	{
		// double buffered
		super(true);
		this.width = width;
		this.height = height;
		setSize(width, height);
		setOpaque(false);
		regions = new ArrayList<>();
		currentSelection = new ArrayList<>();
		enabled = false;
		drawingLine = false;
		erasing = false;
		drawingWidth = 2;
		overlay = null;
		setUp();	
	}
	
	/**
	 * mouse listeners
	 */
	public void setUp()
	{
		addMouseListener(new MouseListener() {
			
			@Override
			public void mouseReleased(MouseEvent e) {
				if (contains(e.getPoint()))
					mouseClicked(e);
			}
			
			@Override
			public void mousePressed(MouseEvent e) {
				// to detect clicks when the mouse is moving
				if (contains(e.getPoint()))
					mouseClicked(e);
			}
			
			@Override
			public void mouseExited(MouseEvent e) {
				setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
				if (overlay != null)
					overlay.setCurrentPoint(null);
			}
			
			@Override
			public void mouseEntered(MouseEvent e) {
				// set the correct cursor
				
				// not enabled and not erasing, then give default cursor
				if (!enabled && !erasing)
				{
					setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
					return;
				}
				if (erasing)
				{
					setCursor(new Cursor(Cursor.HAND_CURSOR));
					//setErasePoint(e.getPoint());
				}
				else if (enabled){
					// set the cursor to be a selection cursor
					setCursor(new Cursor(Cursor.CROSSHAIR_CURSOR));
				}
			}
			
			@Override
			public void mouseClicked(MouseEvent e) {
				// if erasing, then remove shape at that point
				if (erasing){
					removeShapeAtPoint(e.getPoint());
					paintShapesToImage();
					return;
				}
				if (!enabled)
				{
					// not erasing and not enabled for selection
					paintShapesToImage();
					return;
				}
				//System.out.println("Mouse clicked");
				// if drawing a line, then add a new line to the current selection
				if (drawingLine)
				{
					//System.out.println("drawling line");
					// if the current point is the P2 in the last line in the selection
					// then there is no line, and it was likely caused by a double click
					// do nothing and let the second click register to execute the double click code
					if (!currentLineStart.equals(e.getPoint()))
					{
						//System.out.println("valid point... adding line to current selection");
						addLineToCurrentSelection(currentLineStart, e.getPoint());
					}
				} else {
					// if we're NOT in the middle of drawing a line, but we have clicked, then 
					// set the start of the polygon to the current point
					//currentPolygonStart = e.getPoint();
					//System.out.println("NOT drawing line, start a new one");
					setCurrentPolygonStart(e.getPoint());
				}
				//System.out.println("setting drawlingLine to true");
				// we've clicked, so now we ARE drawing a line
				setDrawingLine(true);
				// set the current line start to the current point
				//currentLineStart = e.getPoint();
				setCurrentLineStart(e.getPoint());
				
				// if the click was a double click, then don't add the new line, but instead
				// create a new line from this point to the starting point and add that to the current selection
				if (e.getClickCount() == 2)
				{
					//System.out.println("double clicked!");
					setDrawingLine(false);
					//setCurrentLine(null);
					// was a double click, if the current selection has less then two lines, then
					// it's not a polygon we're interested in
					if (currentSelection.size() < 2)
					{
						
						// repainting just incase something happened
						// the user probably clicked for some reason, maybe they were frustrated and
						// wanted the image to update
						finishedSelection(currentPoint);
//						paintShapesToImage();
						return;
					}

					// was a double click and the currentSelection is valid
					finishedSelection(e.getPoint());
					paintShapesToImage();
				}
			}
		});
		
		addMouseMotionListener(new MouseMotionListener() {
			
			@Override
			public void mouseMoved(MouseEvent e) {
				if ((enabled || erasing) && getBounds().contains(e.getPoint()))
				{
					if (overlay != null)
						overlay.setCurrentPoint(e.getPoint());
				}
				else {
					if (overlay != null)
						overlay.setCurrentPoint(null);
				}
				if (!enabled)
					return;
				if (drawingLine)
					setCurrentLine(new Line2D.Double(currentLineStart, e.getPoint()));
				paintShapesToImage();
			}
			
			@Override
			public void mouseDragged(MouseEvent e) {mouseMoved(e);}
		});
	}
	
	/**
	 * add current line to the current selection
	 */
	public void addLineToCurrentSelection(Point2D start, Point2D end)
	{
		Line2D line2d = new Line2D.Double(start, end);
		if (currentSelection.contains(line2d))
			return;
		currentSelection.add(line2d);
		paintShapesToImage();
	}
	
	/**
	 * converts the current selection into a polygon by connecting the input point 
	 * with the starting point
	 */
	public void finishedSelection(Point2D point2d)
	{
		addCurrentSelectionToRegions();
		setCurrentSelection(new ArrayList<Line2D>());
		setCurrentLine(null);
		paintShapesToImage();
	}
	
	/**
	 * convert the current selection to a shape and add to the shapes list
	 */
	public void addCurrentSelectionToRegions()
	{
		Polygon p = new Polygon();
		if (currentSelection.size() < 2)
		{
			return;
		}
		Line2D line = currentSelection.get(0);
		addPointToPolygon(p, line.getP1());
		for (int i = 1; i < currentSelection.size(); i++)
		{
			addPointToPolygon(p, line.getP2());
			line = currentSelection.get(i);
		}
		addPointToPolygon(p, line.getP2());
		Region region = new Region();
		region.setID("" + regions.size());
		region.setShape(p);
		addRegion(region);
		paintShapesToImage();
	}
	
	private void addPointToPolygon(Polygon poly, Point2D point)
	{
		int x = (int)point.getX();
		int y = (int)point.getY();
		poly.addPoint(x,y);
	}
	
	/**
	 * remove a shape
	 */
	public void removeShapeAtPoint(Point2D point2d)
	{
		for (Region region : regions) {
			if (region.getShape().contains(point2d))
			{
				removeRegion(region);
				paintShapesToImage();
				break;
			}
		}
	}
	
	/**
	 * remove all shapes
	 */
	public void clearData()
	{
        setDrawingLine(false);
		setRegions(new ArrayList<Region>(), true);
		setCurrentSelection(new ArrayList<Line2D>());
		setCurrentLine(null);
		setCurrentPoint(null);
		setCurrentLineStart(null);
		paintShapesToImage();
	}

	/**
	 * paint the shapes and current selection to the imgae
	 * repaint
	 */
	public void paintShapesToImage()
	{
		if (overlay != null)
			overlay.paintShapesToImage();
		selectionsImage = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
		Graphics2D g2 = selectionsImage.createGraphics();
		// initialise the image to be transparent
		g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
                RenderingHints.VALUE_ANTIALIAS_ON);
		g2.setComposite(AlphaComposite.Clear);
		g2.fillRect(0, 0, width, height);
		// now we want to add the shapes, it can no longer be transparent
		g2.setComposite(AlphaComposite.Src);
		g2.setColor(Color.CYAN);
		g2.setStroke(new BasicStroke(drawingWidth));
		
		for (Region region : regions) {
			g2.draw(region.getShape());
		}
		g2.setColor(Color.decode("#1498CC"));
		for (Line2D line2d : currentSelection) {
			g2.draw(line2d);
		}
		g2.setColor(Color.GREEN);
		if (currentLine != null)
			g2.draw(currentLine);
		g2.setColor(Color.RED);
		g2.setFont(new Font("serif", Font.BOLD, 15));
		for (Region region : getRegions()) {
			Point midPoint = ImagingFunctions.getMiddleOfShape(region.getShape());
			g2.drawString(region.getID(), midPoint.x, midPoint.y);
		}
		g2.dispose();
		repaint();
	}

	/**
	 * paint
	 */
	@Override
	public void paintComponent(Graphics g)
	{
		super.paintComponent(g);
		g.drawImage(selectionsImage, 0, 0, null);
	}
	
}
