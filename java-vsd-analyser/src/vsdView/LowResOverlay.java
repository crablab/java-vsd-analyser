package vsdView;

import helpers.ImagingFunctions;

import java.awt.*;
import java.awt.geom.Line2D;
import java.awt.geom.Point2D;
import java.awt.image.BufferedImage;
import java.util.List;

/**
 * the low-resolution image overlay. This will show the scaled down selections made
 * on the high-resolution image
 * @author Andrew Stabler 101748285
 *
 */
public class LowResOverlay extends Overlay {

	private static final long serialVersionUID = 5508929126167869205L;
	private int lowResWidth, lowResHeight;
	private int scale;
	private BufferedImage lowResImage;
	private Color pointerColor;

	public LowResOverlay(int width, int height, int lowResWidth, int lowResHeight, int scale)
	{
		super(width, height);
		this.lowResWidth = lowResWidth;
		this.lowResHeight = lowResHeight;
		this.scale = scale;
		pointerColor = Color.GREEN;
		lowResImage = new BufferedImage(lowResWidth, lowResHeight, BufferedImage.TYPE_INT_ARGB); 
		setDrawingWidth(1);
	}
	
	public void setErasing(boolean erasing)
	{
        if (erasing)
            setPointerColor(Color.RED);
	}
	
	public void setDrawingLine(boolean drawingLine)
	{
		super.setDrawingLine(drawingLine);
        if (drawingLine)
		    setPointerColor(Color.GREEN);
	}
	
	public void setPointerColor(Color inColor)
	{
		if (inColor == null) return;
		pointerColor = inColor;
		paintShapesToImage();
	}
	
	private Point2D scalePoint(Point2D point2d)
	{
		return ImagingFunctions.scalePoint(point2d, getWidth(), getHeight(), lowResWidth, lowResHeight);
	}
	
	private Line2D scaleLine(Line2D line2d)
	{
		Point2D newP1 = scalePoint(line2d.getP1());
		Point2D newP2 = scalePoint(line2d.getP2());
		return new Line2D.Double(newP1, newP2);
	}
	
	public Shape getScaledShape(Shape shape)
	{
		List<Point> points = ImagingFunctions.getPointsFromShape(shape);
		int[] xPoints = new int[points.size()];
		int[] yPoints = new int[points.size()];
		for (int i = 0; i < points.size(); i++) {
			Point2D point = scalePoint(points.get(i));
			xPoints[i] = (int)point.getX(); 
			yPoints[i] = (int)point.getY(); 
		}
		return new Polygon(xPoints, yPoints, xPoints.length);
	}
	
	@Override
	public void paintShapesToImage()
	{
		lowResImage = new BufferedImage(lowResWidth, lowResHeight, BufferedImage.TYPE_INT_ARGB);
		Graphics2D g2 = lowResImage.createGraphics();
		// initialise the image to be transparent
		g2.setComposite(AlphaComposite.Clear);
		g2.fillRect(0, 0, lowResWidth, lowResHeight);
		// now we want to add the shapes, it can no longer be transparent
		g2.setComposite(AlphaComposite.Src);
		g2.setColor(Color.CYAN);
		g2.setStroke(new BasicStroke(getDrawingWidth()));
		// scale down each shape, and draw it
		for (Region region : getRegions()) {
			Shape polygon = getScaledShape(region.getShape());
			g2.draw(polygon);
		}
		g2.setColor(Color.decode("#1498CC"));
		for (Line2D line2d : getCurrentSelection()) {
			line2d = scaleLine(line2d);
			g2.draw(line2d);
		}
		g2.setColor(Color.GREEN);
		if (getCurrentLine() != null){
			Line2D currentLine = scaleLine(getCurrentLine());
			g2.draw(currentLine);
		}
		if (getCurrentPoint()!= null)
		{
			g2.setColor(pointerColor);
			Point2D newCurrentPoint2d = scalePoint(getCurrentPoint());
			g2.draw(new Line2D.Double(newCurrentPoint2d, newCurrentPoint2d));
		}
		int[] imgData = lowResImage.getRGB(0, 0, lowResWidth, lowResHeight, null, 0, lowResWidth);
		int[] resizedImgData = ImagingFunctions.enlargeIntImage(imgData, lowResWidth, lowResHeight, scale);
		BufferedImage newImage = new BufferedImage(getWidth(), getHeight(), BufferedImage.TYPE_INT_ARGB);
		newImage.setRGB(0, 0, getWidth(), getHeight(), resizedImgData, 0, getWidth());
		setSelectionsImage(newImage);
		g2.dispose();
		// draw the labels of each region onto the image
		// we don't want them being distorted so we draw it after the image has been blown up
		g2 = newImage.createGraphics();
		g2.setColor(Color.RED);
		g2.setFont(new Font("serif", Font.BOLD, 15));
		for (Region region : getRegions()) {
			Point midPoint = ImagingFunctions.getMiddleOfShape(region.getShape());
			g2.drawString(region.getID(), midPoint.x, midPoint.y);
		}
		g2.dispose();
		repaint();
	}
	
	@Override
	public void paintComponent(Graphics g)
	{
		super.paintComponent(g);
	}
	
}