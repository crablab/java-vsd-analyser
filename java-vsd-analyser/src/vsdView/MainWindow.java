package vsdView;

import helpers.ImagingFunctions;
import vsdController.VSDController;
import vsdModel.VSDData;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.border.TitledBorder;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.*;
import java.awt.color.ColorSpace;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.image.*;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * The main GUI frame of the software
 *
 * could be refactored much more
 * extract all of the GUI components into their own class to increase modularity and readability
 */
public class MainWindow extends JFrame {

    private VSDData lowResData;
    private VSDData highResData;
    private VSDController controller;

    public HighLowInfo highLowInfoPanel;

    public static final String BACKGROUND_DARK_GREY = "#3B3B3B";
    public static final String BACKGROUND_LIGHT_GREY = "#8A8A8A";
    public static final String BACKGROUND_LIGHTER_GREY = "#DEDEDE";

    public static final int ORIGINAL_HIGH_RES_IMAGE_WIDTH = 376;
    public static final int ORIGINAL_HIGH_RES_IMAGE_HEIGHT = 252;
    public static final int ORIGINAL_LOW_RES_IMAGE_WIDTH = 40;
    public static final int ORIGINAL_LOW_RES_IMAGE_HEIGHT = 28;

    public static final int HIGH_RES_IMAGE_WIDTH = ORIGINAL_HIGH_RES_IMAGE_WIDTH + 8;
    public static final int HIGH_RES_IMAGE_HEIGHT = ORIGINAL_HIGH_RES_IMAGE_HEIGHT + 4;
    public static final int LOW_RES_IMAGE_WIDTH = ORIGINAL_LOW_RES_IMAGE_WIDTH + 8;
    public static final int LOW_RES_IMAGE_HEIGHT = ORIGINAL_LOW_RES_IMAGE_HEIGHT + 4;

    public static final int SCALE = 8;

    public SelectionTools selectionTools;

    private JButton uploadHighRes;
    private JButton uploadLowRes;

    public ImagePanel lowResImagePanel;
    public ImagePanel highResImagePanel;
    public JLabel lowResFileName;
    private JLabel lowResImage;
    public LowResOverlay lowResOverlay;
    private JLabel highResImage;
    public JLabel highResFileName;
    public Overlay highResOverlay;
    private JButton refreshCharts;
    private String defaultDirectory;

    public GraphAdjustments graphAdjustments;
    public ImageAdjustments imageAdjustments;

    /*
     * way of firing the event to upload files after the file chooser window is displayed
     * not actually displayed, just used for their 'doClick()' functionality
     */
    private JButton fireUploadLowRes;
    private JButton fireUploadHighRes;
    public JButton playLowResFrames;
    private static String playLocation = "/resources/play.png";
    private static String stopLocation = "/resources/stop.png";
    public boolean playingLowRes;

    /**
     * Menu components
     */
    private JMenuBar menuBar;
    private JMenu menu;

    public AnalysisPanel analysisPanel;

    /**
     * @param lowResData the lowResData to set
     */
    public void setLowResData(VSDData lowResData) {
        this.lowResData = lowResData;
    }

    /**
     * @param highResData the highResData to set
     */
    public void setHighResData(VSDData highResData) {
        this.highResData = highResData;
    }

    public MainWindow() {
        super("VSD Analyser");
        try {
            setIconImages(getIcons());
        } catch (IOException e) {
            System.out.println("ERROR");
            JOptionPane.showMessageDialog(null, "Error: could not load icons", "ERROR", JOptionPane.ERROR_MESSAGE);
        }

        makeComponents();
        makeMenu();
        makeFrame();
        showFrame();
    }

    public void setController(VSDController controller)
    {
        this.controller = controller;
        highResOverlay.setController(controller);
        lowResOverlay.setController(controller);
    }

    public void setHighResFileLoc(String loc) {
        highResFileName.setText(trim(loc));
    }

    public void setLowResFileLoc(String loc) {
        lowResFileName.setText(trim(loc));
    }

    private String trim(String in) {
        int maxLen = 20;
        if (in.length() <= 20) return in;
        return "..." + in.substring(in.length() - maxLen);

    }

    private List<Image> getIcons() throws IOException {
        ArrayList<Image> icons = new ArrayList<Image>();
        icons.add(ImageIO.read(ClassLoader.getSystemResource("resources/VSDA-small.png")));
        return icons;
    }

    private void makeComponents() {
        selectionTools = new SelectionTools();
       // selectionEnabled = false;
        fireUploadHighRes = new JButton();
        fireUploadLowRes = new JButton();

        uploadHighRes = new JButton("Upload high-res");
        uploadHighRes.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent arg0) {
                String path = getPathFromFileChooser(false);
                if (!path.equals("")) {
                    fireUploadHighRes.setActionCommand(path);
                    fireUploadHighRes.doClick();
                }
            }
        });

        uploadLowRes = new JButton("Upload low-res");
        uploadLowRes.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent arg0) {
                String path = getPathFromFileChooser(true);
                if (!path.equals("")) {
                    fireUploadLowRes.setActionCommand(path);
                    fireUploadLowRes.doClick();
                }
            }
        });

        refreshCharts = new JButton("Refresh Charts");

        highResImage = new JLabel();
        highResImage.setIcon(getBlankImage());
        lowResImage = new JLabel();
        lowResImage.setIcon(getBlankImage());

        lowResFileName = new JLabel();
        highResFileName = new JLabel();

        analysisPanel = new AnalysisPanel();

        lowResOverlay = new LowResOverlay(HIGH_RES_IMAGE_WIDTH, HIGH_RES_IMAGE_HEIGHT, LOW_RES_IMAGE_WIDTH, LOW_RES_IMAGE_HEIGHT, SCALE);
        highResOverlay = new Overlay(HIGH_RES_IMAGE_WIDTH, HIGH_RES_IMAGE_HEIGHT);
        highResOverlay.setLowResOverlay(lowResOverlay);

        highLowInfoPanel = new HighLowInfo();

        playLowResFrames = new JButton();
        playLowResFrames.setIcon(new ImageIcon(getClass().getResource(playLocation)));
        playLowResFrames.setMargin(new Insets(0, 0, 0, 0));
        playLowResFrames.setEnabled(false);
        playingLowRes = false;
        
        graphAdjustments = new GraphAdjustments();

        imageAdjustments = new ImageAdjustments(lowResData, highResData);
    }

    public void addFireHighResListener(ActionListener listener) {
        fireUploadHighRes.addActionListener(listener);
    }

    public void addFireLowResListener(ActionListener listener) {
        fireUploadLowRes.addActionListener(listener);
    }

    public void addRefreshListener(ActionListener listener) {
        refreshCharts.addActionListener(listener);
    }

    public void addPlayListener(ActionListener listener) {
        playLowResFrames.addActionListener(listener);
    }

    /**
     * called when the play frames button is clicked
     * makes sure the play button turns into a stop button and vice versa
     */
    public void togglePlayButton() {
        playLowResFrames.setIcon(new ImageIcon(getClass().getResource(playingLowRes ? playLocation : stopLocation)));
        uploadLowRes.setEnabled(playingLowRes);
        playingLowRes = !playingLowRes;
    }

    public String getPathFromFileChooser(boolean lowRes) {
        // if the default directory has been set, open there, else don't
        JFileChooser chooser;
        if (defaultDirectory != null) {
            File defaultDir = new File(defaultDirectory);
            chooser = defaultDir.exists() && defaultDir.isDirectory() ? new JFileChooser(defaultDir) : new JFileChooser();
        } else {
            chooser = new JFileChooser();
        }

        chooser.setDialogTitle(lowRes ? "Low-Res GSD File" : "High-Res GSD File");
        FileNameExtensionFilter filter = new FileNameExtensionFilter(
                ".GSD Files", "gsd");

        chooser.setFileFilter(filter);
        int returnVal = chooser.showOpenDialog(null);
        if (returnVal == JFileChooser.APPROVE_OPTION) {
            defaultDirectory = chooser.getCurrentDirectory().getAbsolutePath();
            return chooser.getSelectedFile().getPath();
        }
        return "";
    }

    private void makeMenu() {
        menuBar = new JMenuBar();

        menu = new JMenu("File");
        menu.setMnemonic(KeyEvent.VK_F);

        JMenu openMenu = new JMenu("Open");
        openMenu.setMnemonic(KeyEvent.VK_O);

        JMenuItem openLowRes = new JMenuItem("Low-Res");
        openLowRes.setMnemonic(KeyEvent.VK_L);
        openLowRes.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                uploadLowRes.doClick();
            }
        });

        JMenuItem openHighRes = new JMenuItem("High-Res");
        openHighRes.setMnemonic(KeyEvent.VK_H);
        openHighRes.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                uploadHighRes.doClick();
            }
        });


        JMenuItem exit = new JMenuItem("Exit");
        exit.setMnemonic(KeyEvent.VK_E);
        exit.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                System.exit(0);
            }
        });

        openMenu.add(openHighRes);
        openMenu.add(openLowRes);
        menu.add(openMenu);
        menu.addSeparator();
        menu.add(exit);

        JMenu help = new JMenu("Help");
        help.setMnemonic(KeyEvent.VK_H);
        JMenuItem about = new JMenuItem("About");
        about.setMnemonic(KeyEvent.VK_A);
        about.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JOptionPane.showMessageDialog(null,
                        "Team Leader Jannetta Steyn\nCreated by Andy Stabler\nExtended by Benjamin Kidd\n\u00A9 2014 Newcastle University ",
                        "About",
                        JOptionPane.INFORMATION_MESSAGE,
                        new ImageIcon(getClass().getResource("/resources/VSDA-small.png")));
            }
        });

        help.add(about);
      //TODO does this need to exist?
       // menu.add(help);

        menuBar.add(menu);
        menuBar.add(help);
        setJMenuBar(menuBar);
    }

    private void makeFrame() {
		/*
		 * details
		 */

        JPanel detailsPanel = new JPanel();
        detailsPanel.setLayout(new BoxLayout(detailsPanel, BoxLayout.Y_AXIS));

        detailsPanel.setBorder(new TitledBorder("Tool Box"));

        detailsPanel.add(selectionTools);
        detailsPanel.add(graphAdjustments);
        detailsPanel.add(imageAdjustments);

        detailsPanel.add(highLowInfoPanel);
		/*
		 * end details
		 */
		
		/*
		 * image and analysis
		 */
        // main container for the image and analysis
        JPanel imageAndAnalysisPanel = new JPanel();
        imageAndAnalysisPanel.setLayout(new BoxLayout(imageAndAnalysisPanel, BoxLayout.Y_AXIS));
        JScrollPane analysisScroller = new JScrollPane(analysisPanel);
        analysisScroller.getVerticalScrollBar().setUnitIncrement(16);

        JPanel imagesPanel = new JPanel();
        imagesPanel.setBackground(Color.decode(BACKGROUND_LIGHT_GREY));

        JScrollPane imagesMainScroller = new JScrollPane(imagesPanel);
        imagesMainScroller.getVerticalScrollBar().setUnitIncrement(16);

		/*
		 * images section
		 */
        JPanel highResPanel = new JPanel();
        highResPanel.setLayout(new BoxLayout(highResPanel, BoxLayout.Y_AXIS));

        highResImagePanel = new ImagePanel(highResOverlay, highResImage);
        highResPanel.add(highResImagePanel);

        JPanel highResInfoPanel = new JPanel();
        highResInfoPanel.add(uploadHighRes);
        highResInfoPanel.add(highResFileName);
        highResInfoPanel.setBackground(Color.decode(BACKGROUND_LIGHTER_GREY));
        highResPanel.add(highResInfoPanel);

        JPanel lowResPanel = new JPanel();
        lowResPanel.setLayout(new BoxLayout(lowResPanel, BoxLayout.Y_AXIS));

        lowResImagePanel = new ImagePanel(lowResOverlay, lowResImage);

        lowResPanel.add(lowResImagePanel);
        JPanel lowResInfoPanel = new JPanel();
        lowResInfoPanel.add(uploadLowRes);
        lowResInfoPanel.add(lowResFileName);
        lowResInfoPanel.add(playLowResFrames);
        lowResInfoPanel.setBackground(Color.decode(BACKGROUND_LIGHTER_GREY));

        lowResPanel.add(lowResInfoPanel);
        imagesPanel.add(highResPanel);
        imagesPanel.add(lowResPanel);
		
		/*
		 * end images section
		 */

        add(detailsPanel, BorderLayout.WEST);
        JPanel refreshAndCharts = new JPanel();

        refreshAndCharts.setLayout(new BoxLayout(refreshAndCharts, BoxLayout.Y_AXIS));
        refreshAndCharts.add(refreshCharts);
        refreshAndCharts.add(analysisScroller);

        JSplitPane splitPane = new JSplitPane(JSplitPane.VERTICAL_SPLIT, imagesMainScroller, refreshAndCharts);
        splitPane.setOneTouchExpandable(true);
        splitPane.setDividerLocation(300);
        imageAdjustments.setHighResPanel(highResImagePanel);
        imageAdjustments.setLowResPanel(lowResImagePanel);
        add(splitPane);
		/*
		 * end image and analysis
		 */

        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }
//changes default screen size (initial values 1200 700 )
    private void showFrame() {
        setSize(1300, 700);
        setVisible(true);
    }

    public void refreshImages() {
        try {
            if (lowResData != null && lowResData.getMainData() != null && lowResData.getMainData().getFrames().length > 0) {
                lowResImage.setIcon(makeImage(lowResData.getMainData().getFrames()[0].getData(), LOW_RES_IMAGE_WIDTH, LOW_RES_IMAGE_HEIGHT));
            } else {
                // no image loaded
                lowResImage.setIcon(getBlankImage());
            }

            if (highResData != null && highResData.getMainData() != null && highResData.getMainData().getFrames().length > 0) {
                highResImage.setIcon(makeImage(highResData.getMainData().getFrames()[0].getData(), HIGH_RES_IMAGE_WIDTH, HIGH_RES_IMAGE_HEIGHT));
            } else {
                // no image loaded
                highResImage.setIcon(getBlankImage());
            }
        } catch (IOException e) {
            showErrorMessage("Error while loading the images");
        }
        revalidate();
        repaint();
    }

    public static ImageIcon getBlankImage() {
        return new ImageIcon(new BufferedImage(HIGH_RES_IMAGE_WIDTH, HIGH_RES_IMAGE_HEIGHT, BufferedImage.TYPE_INT_RGB));
    }

    /**
     * converts an image into a JLabel
     */
    public static ImageIcon makeImage(short[] data, int width, int height) throws IOException {
        BufferedImage bufferedImage = null;
        if (width != HIGH_RES_IMAGE_WIDTH || height != HIGH_RES_IMAGE_HEIGHT) {
            data = ImagingFunctions.enlargeShortImage(data, width, height, SCALE);
            width = width * SCALE;
            height = height * SCALE;
        }
        bufferedImage = get16bitImage(data, width, height);
        return new ImageIcon(bufferedImage);
    }


    /**
     * converts an array of pixels into an image
     *
     * @param data
     * @param width
     * @param height
     * @return
     */
    private static BufferedImage get16bitImage(short[] data, int width, int height) {

        ColorModel colorModel = new ComponentColorModel(
                ColorSpace.getInstance(ColorSpace.CS_GRAY),
                new int[]{16},
                false,
                false,
                Transparency.OPAQUE,
                DataBuffer.TYPE_USHORT);

        DataBufferUShort db = new DataBufferUShort(data.clone(), data.length);
        WritableRaster raster = Raster.createInterleavedRaster(db, width,
                height, width, 1, new int[1], null);
        return new BufferedImage(colorModel, raster, false, null);
    }

    public void showErrorMessage(String errorString) {
        JOptionPane.showMessageDialog(this, errorString, "Error!", JOptionPane.ERROR_MESSAGE);
    }

}
