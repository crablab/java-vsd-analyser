package vsdView;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.filechooser.FileNameExtensionFilter;

import vsdController.GshReader;

public class InfoPanel extends JPanel {

	private String data;
	private String headerInfo;
	private String formatInfo;
	
	private JButton uploadHeader;
	private JScrollPane infoScroller;
	private JTextArea infoTextArea;
	
	public InfoPanel()
	{
		formatInfo = "";
		headerInfo = "";
		makeComponents();
		//updateData();
		//infoTextArea.setText(data);
		//infoTextArea.setSelectionStart(0);
		setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		add(uploadHeader);
		add(infoScroller);
	}
	
	private void makeComponents()
	{
		uploadHeader = new JButton("Upload header data");
		uploadHeader.addActionListener(new ActionListener() {
			//TODO add pathing
			@Override
			public void actionPerformed(ActionEvent arg0) {
				JFileChooser chooser;
				chooser = new JFileChooser();
				
				chooser.setDialogTitle("Upload Header Info");
			    FileNameExtensionFilter filter = new FileNameExtensionFilter(
			        ".GSH Files", "gsh");

			    chooser.setFileFilter(filter);
			    int returnVal = chooser.showOpenDialog(null);
			    if(returnVal == JFileChooser.APPROVE_OPTION) 
			    {
			    	GshReader reader = new GshReader(chooser.getSelectedFile().getPath());
			    	addHeader(reader.getGshData().toString());
			    }
			    return;
			}
		});
		
		infoTextArea = new JTextArea(30, 20);
		infoScroller = new JScrollPane(infoTextArea); 
		infoTextArea.setEditable(false);
	}
	
	public void addFormat(String format)
	{
		formatInfo = format;
		// we only add a format when we upload a new file
		// we can't get the header data from the file, so the header data
		// that's there is now redundant - remove it
		headerInfo = "";
		updateData();
	}
	
	public void addHeader(String header)
	{
		headerInfo = header;
		updateData();
	}
	
	private void updateData()
	{
		data = formatInfo + (headerInfo != null && !headerInfo.equals("") ? "\n\nHEADER INFO\n\n" : "") + headerInfo;
		infoTextArea.setText(data);
		infoTextArea.setSelectionStart(0);
		infoTextArea.setCaretPosition(0);
	}
}
