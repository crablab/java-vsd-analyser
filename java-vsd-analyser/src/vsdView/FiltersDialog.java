package vsdView;

import helpers.Filters;
import org.jfree.chart.plot.DatasetRenderingOrder;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

/**
 * Extended by Benjamin from 09/07/2014
 * Created by Andy on 27/03/2014.
 */
public class FiltersDialog extends JDialog implements PropertyChangeListener {

    public static final String[] FILTERS = {"", "High Pass", "Low Pass", "Band Pass"};

    private AnalysisChart inChart;
    private AnalysisChart filterChart;
    private JLabel filtersLbl;
    private JComboBox<String> filtersCombo;
    private JSlider filtersSlider;
    private JButton applyBtn, cancelBtn;
    private XYSeries fitlerSeries;

    private JPanel filterOptionsPanel;
    private JPanel acceptCancelPanel;
    private boolean detrended;
    private boolean normalized;
    private boolean updatingChart;

    /**
     * when the chart is updating we don't want to be able to perform filtering
     * so disable functionality while the chart is rendering
     */
    public void setUpdatingChart(boolean updating) {
        filtersCombo.setEnabled(!updating);
        filtersSlider.setEnabled(!updating);
        applyBtn.setEnabled(!updating);
    }

    public FiltersDialog(AnalysisChart inChart) {
        super();
        updatingChart = false;
        setAlwaysOnTop(true);
        setModal(true);
        setSize(new Dimension(600, 600));
        this.inChart = inChart;
        makeComponents();
        makeDiolog();
        setVisible(true);
        detrended = false;
        normalized = false;
    }

    public void makeComponents() {
        filterChart = new AnalysisChart(inChart.getChartLabel(), null, inChart.getSamplingTime());
        filterChart.addPropertyChangeListener(this);
        filtersLbl = new JLabel("Filters:");

        filtersCombo = new <String>JComboBox(FILTERS);
        filtersCombo.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                applyFilter();
            }
        });

        filtersSlider = new JSlider(1, 100, 50);
        filtersSlider.setMajorTickSpacing(10);
        filtersSlider.setMinorTickSpacing(1);
        filtersSlider.setPaintTicks(true);
        filtersSlider.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseReleased(MouseEvent e) {
                applyFilter();
            }
        });

        applyBtn = new JButton("Apply");
        applyBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (fitlerSeries == null) {
                    dispose();
                    return;
                }

                XYSeriesCollection filterSeriesCollection = filterChart.getChart().getXYSeriesCollection();
                XYSeriesCollection inChartSeriesCollection = inChart.getChart().getXYSeriesCollection();

                for (int i = 0; i < filterSeriesCollection.getSeriesCount(); i++) {
                    XYSeries currentSeries = filterSeriesCollection.getSeries(i);
                    if (inChartSeriesCollection.getSeriesIndex(currentSeries.getKey()) == -1) {
                        // is not already there
                        inChartSeriesCollection.addSeries(currentSeries);
                    } else if (!inChartSeriesCollection.getSeries(i).getKey().equals(inChart.getChartLabel())) {
                        // if the series already exists, and is NOT the original data, then re-add it
                        inChartSeriesCollection.removeSeries(i);
                        inChartSeriesCollection.addSeries(currentSeries);

                    }
                }
                inChart.getChart().revalidate();
                dispose();
            }
        });

        cancelBtn = new JButton("Cancel");
        cancelBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                dispose();
            }
        });

        filterOptionsPanel = new JPanel();
        acceptCancelPanel = new JPanel();
    }

    public void makeDiolog() {
        setUpdatingChart(true);
        filterOptionsPanel.add(filtersLbl);
        filterOptionsPanel.add(filtersCombo);
        filterOptionsPanel.add(filtersSlider);

        acceptCancelPanel.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
        acceptCancelPanel.add(applyBtn);
        acceptCancelPanel.add(cancelBtn);

        getContentPane().setLayout(new BoxLayout(getContentPane(), BoxLayout.Y_AXIS));
        getContentPane().add(filterChart.getChartPanel());
        getContentPane().add(filterOptionsPanel);
        getContentPane().add(acceptCancelPanel);

        filterChart.setValues(inChart.getValues());
    }

    public void applyFilter() {
        // need to check we're not currently updating the chart
        if (updatingChart)
            return;
        setUpdatingChart(true);
        filterOptionsPanel.repaint();
        acceptCancelPanel.repaint();
        XYSeriesCollection seriesCollection = filterChart.getChart().getXYSeriesCollection();
        // detrend the data before we do filtering

        switch (filtersCombo.getSelectedIndex()) {
            case 1:
                fitlerSeries = Filters.highPass(seriesCollection.getSeries(inChart.getChartLabel()), filtersSlider.getValue() / 100.0);
                break;
            case 2:
                fitlerSeries = Filters.lowPass(seriesCollection.getSeries(inChart.getChartLabel()), filtersSlider.getValue());
                break;
            case 3:
                fitlerSeries = Filters.bandPass(seriesCollection.getSeries(inChart.getChartLabel()), filtersSlider.getValue(), filtersSlider.getValue() / 100.0);
                break;
            default:
                fitlerSeries = null;
        }
        if (fitlerSeries == null){
            setUpdatingChart(false);
            return;
        }

        // fitlerSeries = Filters.highPass(inChart.getXYSeriesCollection().getSeries(0), filtersSlider.getValue()/100.0);
        // index of the filter series (-1 if not exists)
        int checkSeries = seriesCollection.getSeriesIndex(fitlerSeries.getKey());

        filterChart.getChart().getChart().getXYPlot().setDatasetRenderingOrder(DatasetRenderingOrder.REVERSE);
        if (checkSeries == -1) {
            // if we've already done some filtering, then adjust the already existing filter results
            seriesCollection.addSeries(fitlerSeries);
        } else {
            // removing the old filter series, then adding the new one.
            // possibly really inefficient/slow
            seriesCollection.removeSeries(checkSeries);
            seriesCollection.addSeries(fitlerSeries);

        }
        filterChart.getChart().revalidate();
        setUpdatingChart(false);
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        if (evt.getPropertyName().equals("state")) {
            SwingWorker.StateValue state = (SwingWorker.StateValue) evt.getNewValue();
            if (state.equals(SwingWorker.StateValue.DONE)) {
                // know the event has finished
                setUpdatingChart(false);
            }
        }
    }
}
