package vsdView;

import java.awt.Color;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;

import vsdModel.GsdData;

/**
 * JPanel used to house the high and low resolution header information
 */
public class HighLowInfo extends JPanel {

	private JLabel title;
	private JTabbedPane tabbedPane;
	private InfoPanel highResInfo;
	private InfoPanel lowResInfo;
	
	public HighLowInfo()
	{
		setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		
		highResInfo = new InfoPanel();
		lowResInfo = new InfoPanel();
		tabbedPane = new JTabbedPane();
		
		tabbedPane.addTab("High Res Info", highResInfo);
		tabbedPane.addTab("Low Res Info", lowResInfo);
		add(tabbedPane);
		setBorder(BorderFactory.createLineBorder(Color.decode(MainWindow.BACKGROUND_DARK_GREY)));
	}
	
	public void addHighResData(GsdData highRes)
	{
		highResInfo.addFormat(highRes.getFormInfo().toString());
	}
	
	public void addLowResData(GsdData lowRes)
	{
		lowResInfo.addFormat(lowRes.getFormInfo().toString());
	}
}