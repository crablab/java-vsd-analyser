package vsdView;

import helpers.Filters;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.title.TextTitle;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;

import javax.swing.*;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

/**
 * Class to create JFreeChart Charts using the SwingWorker class
 */
public class AnalysisChart extends SwingWorker<JPanel, String> implements PropertyChangeListener {

    private JPanel mainContainer;
    private JPanel chartOptions;
    private JCheckBox syncZoom;
    private JButton export;
    private JButton filter;
    private JButton clearFilters;
    private JButton clearLastMarker;
    private JButton clearAllMarkers;
    
    
    private FilterVisibilityPanel filterVis;

    private JPanel analysisChart;

    private short[] values;
    private VSDChartPanel vsdChartPanel;
    private JProgressBar progressBar;
    private boolean creatingChart;
    private Point xPoint;
    private String chartLabel;
    private double samplingTime;

    public static boolean autoDetrend;
    public static boolean autoSmooth;
    public static boolean autoNormalize;
    public String getChartLabel() {
        return chartLabel;
    }

    public double getSamplingTime() {
        return samplingTime;
    }

    /**
     * @return the xPoint
     */
    public Point getxPoint() {
        return xPoint;
    }

    /**
     * @param xPoint the xPoint to set
     */
    public void setxPoint(Point xPoint) {
        this.xPoint = xPoint;
        if (vsdChartPanel != null)
            vsdChartPanel.setxPoint(xPoint);
    }

    public short[] getValues() {
        return values;
    }

    public void setValues(short[] values) {
        this.values = values;
        xPoint = new Point(0, 0);
        execute();
    }

    public JPanel getMainChartContainer() {
        return mainContainer;
    }

    /**
     * returns the container which contains the chart and progress bar
     */
    public JPanel getChartPanel() {
        return analysisChart;
    }

    /**
     * @return the actual VSDChartPanel object - the chart object
     */
    public VSDChartPanel getChart() {
        return vsdChartPanel;
    }

    public boolean IsSynced() {
        return syncZoom.isSelected();
    }

    public AnalysisChart(String label, short[] points, double samplingTime) {
    	
        analysisChart = new JPanel(true);
        chartOptions = new JPanel();
        mainContainer = new JPanel(new BorderLayout());
        mainContainer.setBorder(BorderFactory.createLineBorder(Color.black));

        this.chartLabel = label;
        analysisChart.setName(label);
        analysisChart.setLayout(new BoxLayout(analysisChart, BoxLayout.Y_AXIS));

        chartOptions.setLayout(new BoxLayout(chartOptions, BoxLayout.Y_AXIS));
        chartOptions.setBackground(Color.decode(MainWindow.BACKGROUND_LIGHTER_GREY));
        chartOptions.setBorder(BorderFactory.createLineBorder(Color.decode(MainWindow.BACKGROUND_LIGHT_GREY)));

        makeComponents();
        creatingChart = false;
        analysisChart.add(progressBar);
        analysisChart.add(vsdChartPanel);

        chartOptions.add(syncZoom);
        chartOptions.add(export);
        chartOptions.add(filter);
        chartOptions.add(clearFilters);
        chartOptions.add(clearLastMarker);
        chartOptions.add(clearAllMarkers);
       

        JScrollPane filterVisScroller = new JScrollPane(filterVis);
        filterVisScroller.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
        filterVisScroller.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
        filterVisScroller.setPreferredSize(new Dimension(80, 10));
        chartOptions.add(filterVisScroller);

        mainContainer.add(chartOptions, BorderLayout.WEST);
        mainContainer.add(analysisChart, BorderLayout.CENTER);

        addPropertyChangeListener(this);

        this.samplingTime = samplingTime;
        if (points != null) setValues(points);
    }
//NOTE sync zoom
    public void makeComponents() {
        vsdChartPanel = new VSDChartPanel(null);
        vsdChartPanel.setRangeZoomable(false);
        
        vsdChartPanel.setZoomSync(!chartLabel.equals(AnalysisPanel.ANALOG_NAME));
        progressBar = new JProgressBar(0, 100);
        progressBar.setValue(0);
        progressBar.setStringPainted(true);
        analysisChart.setEnabled(false);

        syncZoom = new JCheckBox("Sync Zoom");
        syncZoom.setSelected(vsdChartPanel.isZoomSync());
        syncZoom.setOpaque(false);
        syncZoom.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent arg0) {
                vsdChartPanel.setZoomSync(syncZoom.isSelected());
            }
        });

        export = new JButton("Export (.csv)");
        export.addActionListener(new ActionListener() {
//TODO Export to CSV button - remember path
            @Override
            public void actionPerformed(ActionEvent e) {
            	File defaultDir = vsdChartPanel.getDefaultDirectoryForSaveAs();
                
            	JFileChooser chooser = new JFileChooser();//(defaultDir !=null) && defaultDir.isDirectory() ? new JFileChooser(defaultDir) : new JFileChooser();
            	
                chooser.setDialogTitle("Export " + vsdChartPanel.getChart().getTitle() + " to CSV");
                DateFormat fmt = new SimpleDateFormat("yyyy-MM-dd");
                chooser.setSelectedFile(new File(vsdChartPanel.getChart().getTitle().getText() + "-" + fmt.format(Calendar.getInstance().getTime()) + ".csv"));

                if (chooser.showSaveDialog(mainContainer) == JFileChooser.APPROVE_OPTION) {
                    File sFile = chooser.getSelectedFile();
                    if (sFile.canWrite() || !sFile.exists()) {
                        vsdChartPanel.exportToCSV(new File(sFile.getParent()));
                    }
                    //vsdChartPanel.setDefaultDirectoryForSaveAs(defaultDir);
                }
                return;
            }
        });

        filter = new JButton("Add Filter");
        filter.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                addFilter();
            }
        });

        clearFilters = new JButton("Clear Filters");
        clearFilters.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                clearAllFilters();
            }
        });
        clearLastMarker = new JButton("Clear Last Marker");
        clearLastMarker.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                vsdChartPanel.removeMarker(-1);
                vsdChartPanel.revalidate();
                vsdChartPanel.repaint();
            }
        });
        clearAllMarkers = new JButton("Clear All Markers");
        clearAllMarkers.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
            	vsdChartPanel.removeMarker(-2);
            	vsdChartPanel.revalidate();
                vsdChartPanel.repaint();
            }
        });

        filterVis = new FilterVisibilityPanel(vsdChartPanel);
    }

	private void addFilter() {
        new FiltersDialog(this);
    }

    private void clearAllFilters()
    {
        XYSeriesCollection seriesCollection = vsdChartPanel.getXYSeriesCollection();
        for (int i = seriesCollection.getSeriesCount() - 1; i >= 0; i--) {
            if (!seriesCollection.getSeries(i).getKey().equals(chartLabel))
            {
                // the series is not the original data, so let's remove it
                seriesCollection.removeSeries(i);
            }
        }
        vsdChartPanel.revalidate();
        vsdChartPanel.repaint();
    }
  //NOTE AXIS SCALE?
    @Override
    protected JPanel doInBackground() throws Exception {
        if (creatingChart) return null;
        progressBar.setVisible(true);
        setProgress(1);
        analysisChart.setCursor(new Cursor(Cursor.WAIT_CURSOR));
        final XYSeries series = new XYSeries(chartLabel);
        series.setDescription("original signal");
        vsdChartPanel.getChartRenderingInfo().setEntityCollection(null);

        final JFreeChart chart = ChartFactory.createXYLineChart(
                chartLabel,
                "ms",
                "mV",
                vsdChartPanel.getXYSeriesCollection(),
                PlotOrientation.VERTICAL,
                true,
                true,
                false
        );

        // Create an NumberAxis
        NumberAxis yAxis = new NumberAxis();
        yAxis.setAutoRangeIncludesZero(false);
        
        //NOTE commenting the line below it shows the x axis label
        //((XYPlot) chart.getPlot()).setRangeAxis(yAxis);
        //NOTE domain axis is the X axis 
        chart.setTitle(new TextTitle(chartLabel, new Font("default", Font.BOLD, 16)));
        chart.setBackgroundPaint(Color.decode(MainWindow.BACKGROUND_LIGHTER_GREY));
        chart.getXYPlot().getRenderer().setSeriesPaint(0, Color.decode("#2B2B2B"));
        chart.getXYPlot().getRenderer().setSeriesPaint(1, Color.decode("#1498CC"));
        chart.getXYPlot().getRenderer().setSeriesPaint(2, Color.decode("#9E399E"));
        vsdChartPanel.setChart(chart);

        vsdChartPanel.setHorizontalAxisTrace(true);
        if (values != null) {
            publish("adding values to chart");
            //NOTE sysout back length
            System.out.println("background values.length: "+values.length);
            for (int i = 0; i < values.length; i++) {
                creatingChart = true;
            	//i*samplingTime of 1.5ms
                series.add(i*samplingTime, values[i]);
                int progress = (i + 1) * 98 / values.length;
                if (progress % 10 == 0)
                    setProgress(progress);
            }
            vsdChartPanel.setEnabled(true);
            analysisChart.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
            setProgress(99);
            // make this optional //TODO ?
//TODO add auto smooth
            XYSeries tempSeries= series;
            
            if(autoDetrend) 	tempSeries = Filters.detrend(tempSeries, vsdView.GraphAdjustments.getDetrendWindow());
            if(autoNormalize) 	tempSeries = Filters.normalize(tempSeries, 1, -1);
            if(autoSmooth) 		tempSeries = Filters.smooth(tempSeries, vsdView.GraphAdjustments.getSmoothWindow());
            vsdChartPanel.getXYSeriesCollection().addSeries(tempSeries);
            filterVis.addFilter(chartLabel);
            publish("Complete!");
            creatingChart = false;
            setProgress(100);
        }
        return mainContainer;
    }

    @Override
    public void process(List<String> chunks) {
        vsdChartPanel.revalidate();
        vsdChartPanel.repaint();
    }

    @Override
    protected void done() {
        analysisChart.revalidate();
        analysisChart.repaint();
        vsdChartPanel.revalidate();
        vsdChartPanel.repaint();
        progressBar.setVisible(false);
    }

    public void propertyChange(PropertyChangeEvent evt) {
        if ("progress".equals(evt.getPropertyName())) {
            int progress = (Integer) evt.getNewValue();
            progressBar.setValue(progress);
        }
    }

}
