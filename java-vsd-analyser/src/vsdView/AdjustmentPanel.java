package vsdView;

import javax.swing.*;
import javax.swing.border.TitledBorder;
import java.awt.*;
import java.awt.event.MouseAdapter;

/**
 * Created by Andy on 20/04/2014.
 *
 * JPanel used when we want some adjustment control
 */
public class AdjustmentPanel extends JPanel {

    private String title;
    private JSlider adjustmentSlider;

    public AdjustmentPanel(String title)
    {
        this.title = title;
        setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
        makeComponents();
        addComponents();
    }

    private void makeComponents()
    {
        adjustmentSlider = new JSlider(0, 100, 50);
        adjustmentSlider.setMajorTickSpacing(10);
        adjustmentSlider.setMinorTickSpacing(1);
        adjustmentSlider.setPaintTicks(true);
    }

    public void addComponents()
    {
        setBorder(BorderFactory.createTitledBorder(BorderFactory.createEmptyBorder(), title));
        add(adjustmentSlider);
    }

    public JSlider getSlider()
    {
        return adjustmentSlider;
    }

    public int getSliderVal()
    {
        return adjustmentSlider.getValue();
    }

    public void addSliderListener(MouseAdapter mouseAdapter)
    {
        adjustmentSlider.addMouseListener(mouseAdapter);
    }

    public String getTitle() {
        return title;
    }
}
