package vsdView;

import org.jfree.chart.ChartMouseEvent;
import org.jfree.chart.ChartMouseListener;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.ChartUtilities;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.AxisLocation;
import org.jfree.chart.plot.ValueMarker;
import org.jfree.chart.plot.XYPlot;
import org.jfree.data.xy.XYDataItem;
import org.jfree.data.xy.XYDataset;
import org.jfree.data.xy.XYSeriesCollection;
import org.jfree.ui.RectangleAnchor;
import org.w3c.dom.css.Rect;

import vsdModel.AnalogInputSignal;
import vsdModel.GraphLineData;

import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;

import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.geom.Line2D;
import java.awt.geom.Rectangle2D;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

public class VSDChartPanel extends ChartPanel {

    private XYSeriesCollection seriesCollection;
    private List<GraphLineData> lineList ;

    public XYSeriesCollection getXYSeriesCollection() {
        return seriesCollection;
    }

    /*
     * determines if the chart is in a synchronous zoom mode
     */
    private boolean zoomSync;

    // for the playing of all frames
    private boolean playing;
    private boolean markRemove = false;
    private boolean markRemoveLast = false;
    private int currentTime;
    private ValueMarker videoMarker;
    /**
     * removes chart marker specified
     * if -1 removed last marker
     * if -2 removes all markers
     * @param i
     */
    public void removeMarker(int i){
    	
    	if(lineList==null || lineList.isEmpty())
    		return;
    	if(i== -1){
    		if(lineList.size()<1)return;
    		else
    		lineList.remove(lineList.size()-1);
    		markRemoveLast = true;
    	}
    	else
    	if(i== -2){
    		lineList.clear();
    		markRemove = true;
    	}else
    	lineList.remove(i);
    	
    }

    public boolean isZoomSync() {
        return zoomSync;
    }

    public void setZoomSync(boolean zoom) {
        zoomSync = zoom;
    }

    private Point xPoint;

    /**
     * @param xPoint the xPoint to set
     */
    public void setxPoint(Point xPoint) {
        this.xPoint = xPoint;
        
        repaint();
    }

    public void setPlaying(boolean playing) {
        if (!playing) {
            // if marker isn't already added, nothing should happen (no exception)
            boolean worked = getChart().getXYPlot().removeDomainMarker(videoMarker);
            System.out.println("worked: " + worked);
        } else {
            // need to remove the marker
            this.playing = playing;
            videoMarker.setPaint(Color.RED);
            getChart().getXYPlot().addDomainMarker(videoMarker);
        }
    }

    public void setTime(int time) {
        currentTime = time;
        XYDataset dataset =  getChart().getXYPlot().getDataset();
        videoMarker.setValue((dataset.getX(0, 1).doubleValue() - dataset.getX(0, 0).doubleValue()) * currentTime);
    }

    public VSDChartPanel(JFreeChart chart) {
        super(chart);
        addChartMouseListener(new ChartMouseListener() {
			
			@Override
			public void chartMouseMoved(ChartMouseEvent arg0) {}
			
			@Override
			public void chartMouseClicked(ChartMouseEvent arg0) {
				// TODO Auto-generated method stub
				setxPoint(arg0.getTrigger().getPoint());
			}
		});
        xPoint = new Point(0, 0);
        setDoubleBuffered(true);
        seriesCollection = new XYSeriesCollection();
        videoMarker = new ValueMarker(0);
        videoMarker.setLabelAnchor(RectangleAnchor.CENTER);
        
    }
//TODO This error is thrown ? why
    public void exportToCSV(File outFile) {
        JFreeChart chart = getChart();
        if (chart.getXYPlot().getSeriesCount() < 1) return;

        try (PrintWriter pw = new PrintWriter(outFile)) {
            pw.print(getChart().getXYPlot().getDomainAxis().getLabel() + ",");

            for (int i = 0; i < seriesCollection.getSeriesCount(); i++) {
                pw.print(seriesCollection.getSeries(i).getKey() + ",");
            }

            pw.print("\n");
            for (int i = 0; i < seriesCollection.getItemCount(0); i++) {
                double x = seriesCollection.getSeries(0).getX(i).doubleValue();
                pw.print(x + ",");
                for (int j = 0; j < seriesCollection.getSeriesCount(); j++) {
                    pw.print(seriesCollection.getSeries(j).getY(i).doubleValue() + ",");
                }
                pw.print("\n");
            }
        } catch (IOException e) {
        	Object[] options = {"Close"};
            JOptionPane.showOptionDialog(this, "Error writing chart to .csv file \"" + outFile + "\"", "Error", JOptionPane.OK_OPTION, JOptionPane.ERROR_MESSAGE, null, options, options[0]);
        }
    }

//Blue line that covers the charts on click and prints box with position number
    //Links to VSDChartPanel
    //TODO add multiple lines and display an info box
    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        if(markRemove == true){
        	markRemove = false;
        	return;
        }
        
        Graphics2D g2 = (Graphics2D) g;
        
        if (xPoint == null)
            return;
        //System.out.println(xPoint);
        //79 is the edge x coordinate of the chart left edge of the chart
        //TODO FIX THIS to auto find the point rather than assuming 79 (which is wrong).
        //if(xPoint.getX() <79)xPoint.setLocation(79, xPoint.getY());
        this.getComponentAt((int)xPoint.getX(), (int)xPoint.getY());

        JFreeChart xPos = getChart();
        if (xPos != null){
        	 if(lineList ==null)lineList= new ArrayList<GraphLineData>();
	            GraphLineData gld = new GraphLineData();
	
	        	Rectangle2D plotArea = getScreenDataArea();
	        	XYPlot plot = (XYPlot) getChart().getPlot();
	        	//use this above
	        	double chartX = getChart().getXYPlot().getDomainAxis().java2DToValue(xPoint.x, plotArea, plot.getDomainAxisEdge());
	        	
	        	if (chartX < 0) return;
	        	chartX = Math.round(chartX*100.0)/100.0;
        	if(markRemoveLast != true){
        		

	        	Shape s1 = new Line2D.Double(xPoint.getX(), 0, xPoint.getX(), getHeight());
	        	String label = "" + chartX +" ms";
	        	Rectangle rec = new Rectangle((int)xPoint.getX(), 28, label.length()*7, 15);
	        	
	        	gld.setLine(s1);
	        	gld.setLabelBox(rec);
	        	gld.setLabel(label);
	        	gld.setXPoint(xPoint);
	        	
	        	
	        	
	        	
	        	lineList.add(gld);
	       }
        	markRemoveLast = false;
        	g2.setStroke(new BasicStroke(2));
        	g2.setFont(new Font("default", Font.BOLD, 12));
        	for(GraphLineData gl: lineList){
        		g2.setColor(Color.BLUE);
            	g2.draw(gl .getLine());
            	g2.setColor(Color.WHITE);
            	g2.draw(gl.getLabelBox());
            	g2.fill(gl.getLabelBox());
            	g2.setColor(Color.black);
        	    g2.drawString(gl.getLabel(), (int)gl.getXPoint().getX(),40);
        	}
        }
    }
    
    
    //TODO overwrite save as
    @Override
    public void doSaveAs() throws IOException{
    	JFileChooser fileChooser = new JFileChooser();
                fileChooser.setCurrentDirectory(this.getDefaultDirectoryForSaveAs());
                FileNameExtensionFilter filter = new FileNameExtensionFilter(
                            localizationResources.getString("PNG_Image_Files"), "png");
               fileChooser.addChoosableFileFilter(filter);
                fileChooser.setFileFilter(filter);
        
                int option = fileChooser.showSaveDialog(this);
                if (option == JFileChooser.APPROVE_OPTION) {
                    String filename = fileChooser.getSelectedFile().getPath();
                    if (isEnforceFileExtensions()) {
                        if (!filename.endsWith(".png")) {
                           filename = filename + ".png";
                        }
                   }
                    ChartUtilities.saveChartAsPNG(new File(filename), this.getChart(),
                            getWidth(), getHeight());
                    this.setDefaultDirectoryForSaveAs(new File(fileChooser.getSelectedFile().getParent()));
                }
                
              
    }
    
    
   /* 
    2700     * Opens a file chooser and gives the user an opportunity to save the chart
    2701     * in PNG format.
    2702     *
    2703     * @throws IOException if there is an I/O error.
    2704     
    2705    public void doSaveAs() throws IOException {
    2706        JFileChooser fileChooser = new JFileChooser();
    2707        fileChooser.setCurrentDirectory(this.defaultDirectoryForSaveAs);
    2708        FileNameExtensionFilter filter = new FileNameExtensionFilter(
    2709                    localizationResources.getString("PNG_Image_Files"), "png");
    2710        fileChooser.addChoosableFileFilter(filter);
    2711        fileChooser.setFileFilter(filter);
    2712
    2713        int option = fileChooser.showSaveDialog(this);
    2714        if (option == JFileChooser.APPROVE_OPTION) {
    2715            String filename = fileChooser.getSelectedFile().getPath();
    2716            if (isEnforceFileExtensions()) {
    2717                if (!filename.endsWith(".png")) {
    2718                    filename = filename + ".png";
    2719                }
    2720            }
    2721            ChartUtilities.saveChartAsPNG(new File(filename), this.chart,
    2722                    getWidth(), getHeight());
    2723        }
    2724    }
    
    */
    
    
    
}