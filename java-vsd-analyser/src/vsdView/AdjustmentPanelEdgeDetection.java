package vsdView;

import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by Andy on 30/04/2014.
 * Version of the AdjustmentPanel but is specific to edge detection since includes more components
 */
public class AdjustmentPanelEdgeDetection extends AdjustmentPanel {

    private JRadioButton edgeDetection;

    public AdjustmentPanelEdgeDetection(String title) {
        super(title);
    }

    private void makeComponents() {
        edgeDetection = new JRadioButton("Edge Detect");
        edgeDetection.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                toggleEdgeDetection();
            }
        });
        edgeDetection.setSelected(false);
        toggleEdgeDetection();
    }

    public void addComponents() {
        makeComponents();
        edgeDetection.setAlignmentX(LEFT_ALIGNMENT);
        edgeDetection.setBorder(BorderFactory.createEmptyBorder(0,0,0,0));
        edgeDetection.setOpaque(true);
        add(edgeDetection);
        add(getSlider());
    }

    private void toggleEdgeDetection() {
        getSlider().setEnabled(edgeDetection.isSelected());
    }

    public boolean isDetectionEnabled() {
        return edgeDetection.isSelected();
    }

    public void setDetectionEnabled(boolean enabled)
    {
        edgeDetection.setSelected(enabled);
    }

    public void addEdgeCheckListener(ChangeListener listener)
    {
        edgeDetection.addChangeListener(listener);
    }

}
