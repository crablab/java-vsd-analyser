package vsdView;

import javax.swing.*;
import java.awt.*;
import java.text.DecimalFormat;

/**
 * Panel used to hold the high or low res image, along with its overlay the user can use to select regions to analyse on
 */
public class ImagePanel extends JPanel{
	
	private Overlay overlay;
	private double currentTime;
	private JLabel image;
	
	public ImagePanel(Overlay overlay, JLabel image)
	{
		this.overlay = overlay;
		this.image = image;
		currentTime = 0.00;
		makeComponents();
	}
	
	public void makeComponents()
	{
		this.setLayout(new BorderLayout());
		add(overlay, BorderLayout.CENTER);
		add(image, BorderLayout.CENTER);
		repaint();
		revalidate();
	}
	
	public void updateImageAndTime(ImageIcon imageIcon, double currentTime)
	{
		image.setIcon(imageIcon);
		this.currentTime = currentTime;
		repaint();
	}

    public void updateImage(ImageIcon imageIcon)
    {
        image.setIcon(imageIcon);
        repaint();
    }

	@Override
	public void paint(Graphics g)
	{
		super.paint(g);
		drawInfo(g);
	}
	
	//red time in ms at bottom of video
	public void drawInfo(Graphics g)
	{
		g.setColor(Color.red);
		g.setFont(new Font(Font.SANS_SERIF, Font.BOLD, 15));
		g.drawString(String.valueOf(Double.valueOf((new DecimalFormat("#.##")).format(currentTime))) + "ms", getWidth()-80, getHeight());
	}
}
