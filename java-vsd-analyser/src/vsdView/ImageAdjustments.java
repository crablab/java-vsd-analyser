package vsdView;

import vsdModel.VSDData;

import javax.swing.*;
import javax.swing.border.LineBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import java.awt.*;
import java.awt.event.*;
import java.awt.image.BufferedImage;
import java.awt.image.ConvolveOp;
import java.awt.image.Kernel;
import java.awt.image.RescaleOp;
import java.io.IOException;
import java.util.Arrays;

/**
 * Created by Andy on 20/04/2014.
 */
public class ImageAdjustments extends JPanel {

    //private JCheckBox autoDeTrend;
    private JButton reset;

    private AdjustmentPanel brightnessPanel;
    private AdjustmentPanel sharpnessPanel;
    private AdjustmentPanel contrastPanel;
    private AdjustmentPanelEdgeDetection edgeDetectPanel;

    private VSDData highResData;
    private VSDData lowResData;
    private ImagePanel highResPanel;
    private ImagePanel lowResPanel;

    private boolean enabled;

    public void setHighResData(VSDData highResData) {
        this.highResData = highResData;
    }

    public void setLowResData(VSDData lowResData) {
        this.lowResData = lowResData;
    }

    public void setHighResPanel(ImagePanel highResPanel) {
        this.highResPanel = highResPanel;
    }

    public void setLowResPanel(ImagePanel lowResPanel) {
        this.lowResPanel = lowResPanel;
    }

    // should be pointers to the proper variables.
    public ImageAdjustments(VSDData highResData, VSDData lowResData) {
        this.highResData = highResData;
        this.lowResData = lowResData;

        setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
        enabled = false;
        makeComponents();
        addComponents();
    }

    private void makeComponents() {
        //autoDeTrend = new JCheckBox("Auto Detrend");
        brightnessPanel = new AdjustmentPanel("Brightness");
        JSlider brightnessSlider = brightnessPanel.getSlider();
        brightnessSlider.setPaintLabels(true);
        brightnessSlider.setMajorTickSpacing(10);
        brightnessSlider.setMinorTickSpacing(2);
        brightnessPanel.addSliderListener(new MouseAdapter() {
            @Override
            public void mouseReleased(MouseEvent e) {
                updateImages();
            }
        });

        sharpnessPanel = new AdjustmentPanel("Sharpness");
        JSlider sharpSlider = sharpnessPanel.getSlider();
        sharpSlider.setMinimum(-100);
        sharpSlider.setMaximum(100);
        sharpSlider.setValue(0);
        sharpSlider.setMajorTickSpacing(20);
        sharpSlider.setMinorTickSpacing(5);
        sharpSlider.setPaintLabels(true);
        sharpSlider.repaint();
        sharpnessPanel.addSliderListener(new MouseAdapter() {
            @Override
            public void mouseReleased(MouseEvent e) {
                updateImages();
            }
        });

        contrastPanel = new AdjustmentPanel("Contrast");
        contrastPanel.addSliderListener(new MouseAdapter() {
            @Override
            public void mouseReleased(MouseEvent e) {
                updateImages();
            }
        });
        JSlider contrastSlider = contrastPanel.getSlider();
        contrastSlider.setPaintLabels(true);
        contrastSlider.setMajorTickSpacing(10);
        contrastSlider.setMinorTickSpacing(2);

        edgeDetectPanel = new AdjustmentPanelEdgeDetection("Edge Detect");
        edgeDetectPanel.addSliderListener(new MouseAdapter() {
            @Override
            public void mouseReleased(MouseEvent e) {
                updateImages();
            }
        });
        JSlider edgeSlider = edgeDetectPanel.getSlider();
        edgeSlider.setMinimum(1);
        edgeSlider.setPaintLabels(true);
        edgeSlider.setMajorTickSpacing(10);
        edgeSlider.setMinorTickSpacing(2);
        edgeDetectPanel.addEdgeCheckListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                updateImages();
            }
        });

        reset = new JButton("Reset Images");
        reset.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                resetSliders();
            }
        });
        setBorder(BorderFactory.createTitledBorder("Image Adjustments"));//new LineBorder(Color.LIGHT_GRAY));
    }

    public void resetSliders() {
        brightnessPanel.getSlider().setValue((brightnessPanel.getSlider().getMaximum() + brightnessPanel.getSlider().getMinimum()) / 2);
        contrastPanel.getSlider().setValue((contrastPanel.getSlider().getMaximum() + contrastPanel.getSlider().getMinimum()) / 2);
        sharpnessPanel.getSlider().setValue((sharpnessPanel.getSlider().getMaximum() + sharpnessPanel.getSlider().getMinimum()) / 2);
        edgeDetectPanel.getSlider().setValue((edgeDetectPanel.getSlider().getMaximum() + edgeDetectPanel.getSlider().getMinimum()) / 2);
        edgeDetectPanel.setDetectionEnabled(false);
        updateImages();
    }

    private ImageIcon getImageIcon(VSDData data, int width, int height) {
        ImageIcon image = null;
        try {
            // get the images
            if (data != null)
                image = MainWindow.makeImage(data.getMainData().getFrames()[0].getData(), width, height);
        } catch (IOException e1) {
            e1.printStackTrace();
        }
        return image;
    }

    private ImageIcon brightenImageIcon(ImageIcon image) {
        if (image == null)
            return null;
        BufferedImage bi = new BufferedImage(image.getIconWidth(), image.getIconHeight(), BufferedImage.TYPE_INT_RGB);
        Graphics bi_G = bi.createGraphics();
        image.paintIcon(null, bi_G, 0, 0);

        RescaleOp rescaleOp = new RescaleOp((float) ((brightnessPanel.getSliderVal()) / 10), 0, null);//5f, 40, null);
        rescaleOp.filter(bi, bi);

        bi_G.dispose();
        return new ImageIcon(bi);
    }

    private ImageIcon contrastImageIcon(ImageIcon image) {
        if (image == null)
            return null;

        BufferedImage bi = new BufferedImage(image.getIconWidth(), image.getIconHeight(), BufferedImage.TYPE_INT_RGB);
        Graphics bi_G = bi.createGraphics();
        image.paintIcon(null, bi_G, 0, 0);

        float val = contrastPanel.getSliderVal();
        val = val / 50.0f;
        val *= val;

        RescaleOp rescale = new RescaleOp(val, 5f, null);
        rescale.filter(bi, bi);
        return new ImageIcon(bi);
    }

    private ImageIcon edgeDetectImageIcon(ImageIcon image) {
        if (image == null)
            return null;

        BufferedImage bi = new BufferedImage(image.getIconWidth(), image.getIconHeight(), BufferedImage.TYPE_INT_RGB);
        Graphics bi_G = bi.createGraphics();
        image.paintIcon(null, bi_G, 0, 0);

        float val = (float) 0.1 * edgeDetectPanel.getSliderVal();
        val = val < 0 ? 0.1f : val;

        Kernel kernel = createKernel(val, -1);
        ConvolveOp edgeOp = new ConvolveOp(kernel, ConvolveOp.EDGE_NO_OP, null);
        bi = edgeOp.filter(bi, null);

        return new ImageIcon(bi);
    }

    private ImageIcon sharpenImageIcon(ImageIcon image) {
        if (!enabled || image == null)
            return MainWindow.getBlankImage();
        BufferedImage bi = new BufferedImage(image.getIconWidth(), image.getIconHeight(), BufferedImage.TYPE_INT_RGB);
        Graphics bi_G = bi.createGraphics();
        image.paintIcon(null, bi_G, 0, 0);


        double center;
        double neighbours;
        double sliderVal = (double) sharpnessPanel.getSliderVal();

        if (sliderVal <= -1.0) {
            // blur
            // need to negate slider val to make it positive
            sliderVal = -sliderVal;
            center = 1.0 - Math.log10(sliderVal) / 2;
            neighbours = (1.0 - center) / 8.0;
        } else if (sliderVal > -1.0 && sliderVal <= 1.0) {
            // do nothing in this range
            center = 1.0;
            neighbours = 0.0;
        } else {
            // sharpen
            double sensitivity = 2.0;
            center = (1.0 + Math.log10(sliderVal) * sensitivity);
            neighbours = (1.0 - center) / 8.0;
        }

        Kernel kernel = createKernel((float) center, (float) neighbours);

        ConvolveOp op = new ConvolveOp(kernel);
        bi = op.filter(bi, null);
        bi_G.dispose();
        return new ImageIcon(bi);
    }

    /**
     * given the center weight and value of all neighbouring weights, a Kernel object with these values
     * is returned
     *
     * @param centerWeight
     * @param neighbourWeights
     * @return
     */
    private static Kernel createKernel(float centerWeight, float neighbourWeights) {
        float[] data = new float[9];
        int start = 0;
        int end = 8;
        int mid = 4;
        for (int i = start; i < end; i++) {
            data[i] = (i == mid ? centerWeight : neighbourWeights);
        }
        return new Kernel(3, 3, data);
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
        brightnessPanel.setEnabled(enabled);
        sharpnessPanel.setEnabled(enabled);
    }

    private void addComponents() {
        add(brightnessPanel);
        add(sharpnessPanel);
        add(contrastPanel);
        add(edgeDetectPanel);
        add(reset);
    }

    public void updateImages() {
        if (!enabled)
            return;
        updateHighRes();
        updateLowRes();
    }

    public void updateLowRes() {
        if (!enabled)
            return;
        // get the original data
        ImageIcon lowResImage = getImageIcon(lowResData, MainWindow.LOW_RES_IMAGE_WIDTH, MainWindow.LOW_RES_IMAGE_HEIGHT);
        // perform all imaging functions on the image
        lowResImage = brightenImageIcon(lowResImage);
        lowResImage = sharpenImageIcon(lowResImage);
        lowResImage = contrastImageIcon(lowResImage);
        lowResImage = edgeDetectPanel.isDetectionEnabled() ? edgeDetectImageIcon(lowResImage) : lowResImage;
        lowResPanel.updateImage(lowResImage);
    }

    public void updateHighRes() {
        if (!enabled)
            return;
        // get the original data
        ImageIcon highResImage = getImageIcon(highResData, MainWindow.HIGH_RES_IMAGE_WIDTH, MainWindow.HIGH_RES_IMAGE_HEIGHT);
        // perform all imaging functions on the image
        highResImage = brightenImageIcon(highResImage);
        highResImage = sharpenImageIcon(highResImage);
        highResImage = contrastImageIcon(highResImage);
        highResImage = edgeDetectPanel.isDetectionEnabled() ? edgeDetectImageIcon(highResImage) : highResImage;
        highResPanel.updateImage(highResImage);
    }


}
